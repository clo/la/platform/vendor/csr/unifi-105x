/*
 * Bus I/O operations.
 *
 * Copyright (C) 2007 Cambridge Silicon Radio Ltd.
 *
 * Refer to LICENSE.txt included with this source code for details on
 * the license terms.
 */
#include <sdioemb/cspi.h>

#include "sdio_layer.h"

enum sdio_cmd_direction {
    CMD_WRITE, CMD_READ,
};

static int cmd_status_to_errno(struct sdio_cmd *cmd)
{
    enum sdio_cmd_status status = cmd->status;

    /* Check error bits in valid R5 responses. */
    if (status == SDD_CMD_OK
        && ((cmd->flags & SDD_CMD_FLAG_RESP_MASK) == SDD_CMD_FLAG_RESP_R5
            || (cmd->flags & SDD_CMD_FLAG_RESP_MASK) == SDD_CMD_FLAG_RESP_R5B)) {
        if (cmd->sdio.response.r5 & (SDIO_R5_OUT_OF_RANGE | SDIO_R5_FUNCTION_NUMBER)) {
            return -EINVAL;
        }
        if (cmd->sdio.response.r5 & SDIO_R5_ERROR) {
            return -EIO;
        }
    }

    if (status & SDD_CMD_ERR_TIMEOUT) {
        return -ETIMEDOUT;
    }
    if (status & (SDD_CMD_ERR_CRC | SDD_CMD_ERR_OTHER)) {
        return -EIO;
    }
    if (status & SDD_CMD_ERR_NO_CARD) {
        return -ENODEV;
    }
    return 0;
}

static void wait_for_cmd_complete(struct sdio_cmd *cmd)
{
    os_semaphore_t *cmd_done_sem = cmd->priv;

    os_semaphore_wait(cmd_done_sem);
}

static void cmd_callback(struct sdio_cmd *cmd)
{
    os_semaphore_t *cmd_done_sem = cmd->priv;

    os_semaphore_post(cmd_done_sem);
}

static int start_cmd_and_wait(struct sdio_dev *fdev, struct sdio_cmd *cmd)
{
    os_semaphore_t cmd_done_sem;
    int ret;

    os_semaphore_init(&cmd_done_sem);

    cmd->callback = cmd_callback;
    cmd->priv     = &cmd_done_sem;

    ret = sdio_start_cmd(fdev, cmd);
    if (ret) {
        return ret;
    }
    wait_for_cmd_complete(cmd);

    return 0;
}

static int sdio_cmd52(struct sdio_dev *fdev, int func, uint32_t addr, uint8_t *data,
                      enum sdio_cmd_direction dir)
{
    struct sdio_cmd cmd;
    int ret;
    
    cmd.sdio.cmd = 52;
    cmd.sdio.arg = SDIO_CMD52_ARG_FUNC(func) | SDIO_CMD52_ARG_ADDR(addr)
        | (dir == CMD_WRITE ? SDIO_CMD52_ARG_WRITE | SDIO_CMD52_ARG_DATA(*data) : 0);
    cmd.data  = NULL;
    cmd.flags = SDD_CMD_FLAG_RESP_R5;

    ret = start_cmd_and_wait(fdev, &cmd);
    if (ret) {
        return ret;
    }

    if (cmd.status == SDD_CMD_OK) {
        *data = SDIO_R5_DATA(cmd.sdio.response.r5);
    }
    return cmd_status_to_errno(&cmd);
}

int sdio_io_read8(struct sdio_dev *fdev, int func, uint32_t addr, uint8_t *data)
{
    return sdio_cmd52(fdev, func, addr, data, CMD_READ);
}

int sdio_io_write8(struct sdio_dev *fdev, int func, uint32_t addr, uint8_t data)
{
    return sdio_cmd52(fdev, func, addr, &data, CMD_WRITE);
}

static void sdio_io_abort(struct sdio_dev *fdev, int func)
{
    struct sdio_cmd cmd;
    
    cmd.sdio.cmd = 52;
    cmd.sdio.arg = SDIO_CMD52_ARG_FUNC(0) | SDIO_CMD52_ARG_ADDR(SDIO_CCCR_IO_ABORT)
        | SDIO_CMD52_ARG_WRITE | SDIO_CMD52_ARG_DATA(func);
    cmd.data  = NULL;
    cmd.flags = SDD_CMD_FLAG_RESP_R5 | SDD_CMD_FLAG_ABORT;

    start_cmd_and_wait(fdev, &cmd);
}

static int sdio_cmd53(struct sdio_dev *fdev, int func, uint32_t addr, uint8_t *data,
                      size_t len, enum sdio_cmd_direction dir)
{
    struct sdio_cmd cmd;
    int count;
    int ret;
    
    if (len >= fdev->blocksize) {
        count = len / fdev->blocksize;
    } else {
        count = len;
    }

    cmd.sdio.cmd = 53;
    cmd.sdio.arg = SDIO_CMD53_ARG_FUNC(func) | SDIO_CMD53_ARG_ADDR(addr) | SDIO_CMD53_ARG_CNT(count)
        | (dir == CMD_WRITE ? SDIO_CMD53_ARG_WRITE : 0)
        | (len >= fdev->blocksize ? SDIO_CMD53_ARG_BLK_MODE : 0);
    cmd.data  = data;
    cmd.len   = len;
    cmd.flags = (dir == CMD_READ
                 ? SDD_CMD_FLAG_RESP_R5 | SDD_CMD_FLAG_READ
                 : SDD_CMD_FLAG_RESP_R5B);

    ret = start_cmd_and_wait(fdev, &cmd);
    if (ret) {
        return ret;
    }
    /*
     * If the command completed with an error abort the data transfer
     * with a write to the I/O Abort register, otherwise the transfer
     * state of the card and the host is undetermined.
     */
    if (cmd.status != SDD_CMD_OK && cmd.status != SDD_CMD_ERR_NO_CARD) {
        sdio_io_abort(fdev, func);
    }

    return cmd_status_to_errno(&cmd);
}

int sdio_io_write(struct sdio_dev *fdev, int func, uint32_t addr, const uint8_t *data, size_t len)
{
    int remainder = 0;
    int ret;

    if (len > fdev->blocksize) {
        remainder = len % fdev->blocksize;
        len -= remainder;
    }

    /* The slot drivers won't write to 'data' for a write command so
       casting away the const is safe. */
    ret = sdio_cmd53(fdev, func, addr, (uint8_t *)data, len, CMD_WRITE);
    if (!ret && remainder) {
        ret = sdio_cmd53(fdev, func, addr, (uint8_t *)data + len, remainder, CMD_WRITE);
    }
    return ret;
}

int sdio_io_read(struct sdio_dev *fdev, int func, uint32_t addr, uint8_t *data, size_t len)
{
    int remainder = 0;
    int ret;

    if (len > fdev->blocksize) {
        remainder = len % fdev->blocksize;
        len -= remainder;
    }

    ret = sdio_cmd53(fdev, func, addr, data, len, CMD_READ);
    if (!ret && remainder) {
        ret = sdio_cmd53(fdev, func, addr, data + len, remainder, CMD_READ);
    }
    return ret;
}

static int cspi_word(struct sdio_dev *fdev, int func, uint32_t addr, uint16_t *word, uint8_t dir)
{
    struct sdio_cmd cmd;
    int ret;

    cmd.cspi.cmd  = dir | CSPI_FUNC(func);
    cmd.cspi.addr = addr;
    cmd.cspi.val  = *word;
    cmd.data  = NULL;
    cmd.flags = SDD_CMD_FLAG_CSPI;

    ret = start_cmd_and_wait(fdev, &cmd);
    if (ret) {
        return ret;
    }
    *word = cmd.cspi.val;
    return cmd_status_to_errno(&cmd);
}

int cspi_io_read8(struct sdio_dev *fdev, int func, uint32_t addr, uint8_t *data)
{
    uint16_t word;
    int ret;

    ret = cspi_word(fdev, func, addr, &word, CSPI_READ);
    *data = word & 0xff;
    return ret;
}

int cspi_io_write8(struct sdio_dev *fdev, int func, uint32_t addr, uint8_t data)
{
    uint16_t word = data;

    return cspi_word(fdev, func, addr, &word, CSPI_WRITE);
}

static int cspi_burst(struct sdio_dev *fdev, int func, uint32_t addr, uint8_t *data, size_t len,
                      uint8_t dir)
{
    struct sdio_cmd cmd;
    int ret;

    cmd.cspi.cmd  = dir | CSPI_BURST | CSPI_FUNC(func);
    cmd.cspi.addr = addr;
    cmd.data      = data;
    cmd.len       = len;
    cmd.flags     = SDD_CMD_FLAG_CSPI;

    ret = start_cmd_and_wait(fdev, &cmd);
    if (ret) {
        return ret;
    }

    return cmd_status_to_errno(&cmd);
}

int cspi_io_read(struct sdio_dev *fdev, int func, uint32_t addr, uint8_t *data, size_t len)
{
    return cspi_burst(fdev, func, addr, data, len, CSPI_READ);
}

int cspi_io_write(struct sdio_dev *fdev, int func, uint32_t addr, const uint8_t *data, size_t len)
{
    return cspi_burst(fdev, func, addr, (uint8_t *)data, len, CSPI_WRITE);
}


/*
 * The sdio_raw_*() functions bypass the command queues and send the
 * command directly to the slot driver.
 */

static int start_raw_cmd_and_wait(struct sdio_slot *slot, struct sdio_cmd *cmd)
{
    os_semaphore_t cmd_done_sem;
    int ret;

    os_semaphore_init(&cmd_done_sem);

    cmd->callback = cmd_callback;
    cmd->priv     = &cmd_done_sem;

    ret = slot_start_cmd(slot, cmd);
    if (ret) {
        return ret;
    }
    wait_for_cmd_complete(cmd);

    return 0;
}


int sdio_raw_cmd(struct sdio_slot *slot, uint8_t cmd_id, uint32_t arg, unsigned flags,
                 union sdio_response *response)
{
    struct sdio_cmd cmd;
    int ret;

    cmd.sdio.cmd = cmd_id;
    cmd.sdio.arg = arg;
    cmd.data     = NULL;
    cmd.flags    = SDD_CMD_FLAG_RAW | flags;

    ret = start_raw_cmd_and_wait(slot, &cmd);
    if (ret) {
        return ret;
    }

    if (cmd.status == SDD_CMD_OK) {
        *response = cmd.sdio.response;
    }
    return cmd_status_to_errno(&cmd);
}

int cspi_raw_word_cmd(struct sdio_slot *slot, uint8_t cmd_id, uint32_t addr, uint16_t *word)
{
    struct sdio_cmd cmd;
    int ret;

    cmd.cspi.cmd  = cmd_id;
    cmd.cspi.addr = addr;
    cmd.cspi.val  = *word;
    cmd.data      = NULL;
    cmd.flags     = SDD_CMD_FLAG_RAW | SDD_CMD_FLAG_CSPI;

    ret = start_raw_cmd_and_wait(slot, &cmd);
    if (ret) {
        return ret;
    }
    *word = cmd.cspi.val;
    return cmd_status_to_errno(&cmd);
}


/**
 * Read an 8 bit wide register.
 *
 * @param fdev SDIO function to read from.
 * @param addr register address.
 * @param val  returns the value read.
 *
 * @return 0 on success.
 * @return -EIO if a low-level transport error occurred (e.g., CRC error),
 * @return -ETIMEDOUT if no response was received.
 *
 * @ingroup fdriver
 */
int sdio_read8(struct sdio_dev *fdev, uint32_t addr, uint8_t *val)
{
    return fdev->io->read8(fdev, fdev->function, addr, val);
}

/**
 * Write an 8 bit wide register.
 *
 * @param fdev SDIO function to write to.
 * @param addr register address.
 * @param val  value to write.
 *
 * @return 0 on success.
 * @return -EIO if a low-level transport error occurred (e.g., CRC error),
 * @return -ETIMEDOUT if no response was received.
 *
 * @ingroup fdriver
 */
int sdio_write8(struct sdio_dev *fdev, uint32_t addr, uint8_t val)
{
    return fdev->io->write8(fdev, fdev->function, addr, val);
}

/**
 * Read an 8 bit wide function 0 register.
 *
 * @param fdev an SDIO function of the card.
 * @param addr register address.
 * @param val  returns the value read.
 *
 * @return 0 on success.
 * @return -EIO if a low-level transport error occurred (e.g., CRC error),
 * @return -ETIMEDOUT if no response was received.
 *
 * @ingroup fdriver
 */
int sdio_f0_read8(struct sdio_dev *fdev, uint32_t addr, uint8_t *val)
{
    return fdev->io->read8(fdev, 0, addr, val);
}

/**
 * Write an 8 bit wide function 0 register.
 *
 * @param fdev an SDIO function of the card.
 * @param addr register address.
 * @param val  value to write.
 *
 * @return 0 on success.
 * @return -EIO if a low-level transport error occurred (e.g., CRC error),
 * @return -ETIMEDOUT if no response was received.
 *
 * @ingroup fdriver
 */
int sdio_f0_write8(struct sdio_dev *fdev, uint32_t addr, uint8_t val)
{
    return fdev->io->write8(fdev, 0, addr, val);
}

/**
 * Read a buffer from an 8 bit wide card register/FIFO.
 *
 * @param fdev SDIO function to read from.
 * @param addr register/FIFO address.
 * @param data buffer to store the data read.
 * @param len  length of the buffer.
 *
 * @return 0 on success.
 * @return -EIO if a low-level transport error occurred (e.g., CRC error).
 * @return -ETIMEDOUT if no response or data was received.
 *
 * @ingroup fdriver
 */
int sdio_read(struct sdio_dev *fdev, uint32_t addr, void *data, size_t len)
{
    return fdev->io->read(fdev, fdev->function, addr, data, len);
}

/**
 * Writes a buffer to an 8 bit wide card register/FIFO.
 *
 * @param fdev SDIO function to write to.
 * @param addr register/FIFO address.
 * @param data buffer of data to write.
 * @param len  length of the buffer.
 *
 * @return 0 on success.
 * @return -EIO if a low-level transport error occurred (e.g., CRC error).
 * @return -ETIMEDOUT if no response was received.
 *
 * @ingroup fdriver
 */

int sdio_write(struct sdio_dev *fdev, uint32_t addr, const void *data, size_t len)
{
    return fdev->io->write(fdev, fdev->function, addr, data, len);
}

