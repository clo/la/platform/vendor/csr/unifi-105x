/*
 * 003_cmd52_async - test CMD52 asynchronous reads and writes.
 *
 * Copyright (C) 2008 Cambridge Silicon Radio Ltd.
 *
 * Refer to LICENSE.txt included with this source code for details on
 * the license terms.
 *
 * Tests asynchronous CMD52 reads and writes using the
 * CSR_FROM_HOST_SCRATCH0 register.
 */
#include <string.h>

#include "sdd_test.h"

static const struct sdd_test_card supported_cards[] = {
    { SDIO_MANF_ID_CSR, SDIO_CARD_ID_CSR_BC6, "BC6", },
    { SDIO_MANF_ID_CSR, SDIO_CARD_ID_CSR_DASH_D00, "dash (d00)", },
    { SDIO_MANF_ID_CSR, SDIO_CARD_ID_CSR_DASH, "dash", },
    { },
};

struct state {
    int i;
    uint8_t data;
};

static void cmd52_write_done(sdio_uif_t uif, void *arg, int status);
static void cmd52_read_done(sdio_uif_t uif, void *arg, int status);

static void test_003_cmd52_async(sdio_uif_t uif, void *arg)
{
    struct state *state;
    int ret;

    sdd_test_check_cards(supported_cards);

    state = malloc(sizeof(struct state));

    state->i = 0;
    state->data = 0xff;

    ret = sdio_write8_async(uif, 0, SDIO_CSR_FROM_HOST_SCRATCH0, state->i & 0xff,
                            cmd52_write_done, state);
    if (ret < 0) {
        sdd_test_fail("sdio_write8_async");
    }
}

static void cmd52_write_done(sdio_uif_t uif, void *arg, int status)
{
    struct state *state = arg;
    int ret;
    
    if (status < 0) {
        sdd_test_fail("CMD52 write failed: %s", strerror(-status));
    }

    ret = sdio_read8_async(uif, 0, SDIO_CSR_FROM_HOST_SCRATCH0, &state->data,
                           cmd52_read_done, state);
    if (ret < 0) {
        sdd_test_fail("sdio_read8_async");
    }
}

static void cmd52_read_done(sdio_uif_t uif, void *arg, int status)
{
    struct state *state = arg;
    int ret;

    if (status < 0) {
        sdd_test_fail("CMD52 read failed: %s", strerror(-status));
    }
    if (state->data != (state->i & 0xff)) {
        sdd_test_fail("data read (%d) != expected value (%d)", state->data, state->i & 0xff);
    }

    if (++state->i < sdd_test_max_iters()) {
        ret = sdio_write8_async(uif, 0, SDIO_CSR_FROM_HOST_SCRATCH0, state->i & 0xff,
                                cmd52_write_done, state);
        if (ret < 0) {
            sdd_test_fail("sdio_write8_async");
        }
    } else {
        sdd_test_pass();
    }
}

int main(int argc, char *argv[])
{
    return sdd_test_run(SDD_TEST_NAME, argc, argv,
                        test_003_cmd52_async, NULL, NULL, NULL);
}
