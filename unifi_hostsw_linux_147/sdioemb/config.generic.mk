KDIR   ?= /lib/modules/$(shell uname -r)/build
prefix ?= /usr/local

modules:
	$(MAKE) -C $(KDIR) M=$(BUILDDIR) \
		O=$(O) V=$(V)

install_modules:
	$(MAKE) -C $(KDIR) M=$(BUILDDIR) modules_install INSTALL_MOD_PATH=$(DESTDIR) \
		O=$(O) V=$(V)

clean_modules:
	$(MAKE) -C $(KDIR) M=$(BUILDDIR) clean \
		O=$(O) V=$(V)

ifneq ($(CROSS_COMPILE),)
export CROSS_COMPILE
endif

ifneq ($(ARCH),)
export ARCH
endif

export prefix
