/*
 * Manage a log of SDIO events (commands and interrupts etc.).
 *
 * Copyright (C) 2007 Cambridge Silicon Radio Ltd.
 *
 * Refer to LICENSE.txt included with this source code for details on
 * the license terms.
 */
#include <stdarg.h>

#include <sdioemb/cspi.h>
#include "sdio_layer.h"

#ifdef SDD_DEBUG_EVENT_LOG

#if SDD_DEBUG_DATA_BUFFER_LEN > 0

static void init_data_list(struct sdio_slot *slot)
{
    struct sdio_slot_priv *slotp = slot->priv;

    slotp->event_log.data_buffer = os_alloc_big(SDD_DEBUG_DATA_BUFFER_LEN);
    if (slotp->event_log.data_buffer == NULL) {
        slot_warning(slot, "event log data buffer unavailable");
    }

    slotp->event_log.data_total = slotp->event_log.data_start = 0;
    slotp->event_log.data_head = slotp->event_log.data_tail = NULL;   
}

static void deinit_data_list(struct sdio_slot *slot)
{
    struct sdio_slot_priv *slotp = slot->priv;

    os_free_big(slotp->event_log.data_buffer);
}

uint8_t sdio_event_log_get_data(struct sdio_event_entry *e, unsigned i)
{
    unsigned off;

    if (i >= e->cmd.data.length) {
        return 0;
    }
    off = (e->cmd.data.start + i) % SDD_DEBUG_DATA_BUFFER_LEN;
    return e->cmd.data.buffer[off];
}

/*
 * Remove the data element from the head of the data list. i.e.,
 * remove the oldest data entry.
 */
static void shrink_data_list(struct sdio_slot *slot)
{
    struct sdio_slot_priv *slotp = slot->priv;

    slotp->event_log.data_total -= slotp->event_log.data_head->cmd.data.length;
    slotp->event_log.data_head->cmd.data.length = 0;
    slotp->event_log.data_head = slotp->event_log.data_head->cmd.data.next;
}

static void add_data(struct sdio_slot *slot,  struct sdio_event_entry *entry,
                     const uint8_t *data, unsigned length)
{
    unsigned l1, l2;
    struct sdio_slot_priv *slotp = slot->priv;
    unsigned start = slotp->event_log.data_start;

    if (slotp->event_log.data_buffer == NULL) {
        return;
    }

    if (length > SDD_DEBUG_DATA_BUFFER_LEN) {
        data += length - SDD_DEBUG_DATA_BUFFER_LEN;
        length = SDD_DEBUG_DATA_BUFFER_LEN;
    }

    l1 = min(length, SDD_DEBUG_DATA_BUFFER_LEN - start);
    l2 = length - l1;

    memcpy(slotp->event_log.data_buffer + start, data, l1);
    if (l2 != 0)
        memcpy(slotp->event_log.data_buffer, data + l1, l2);

    entry->cmd.data.buffer = slotp->event_log.data_buffer;
    entry->cmd.data.start = start;
    entry->cmd.data.length = length;
    entry->cmd.data.next = NULL;

    if (slotp->event_log.data_head == NULL) {
        slotp->event_log.data_head = entry;
    } else {
        slotp->event_log.data_tail->cmd.data.next = entry;
    }
    slotp->event_log.data_tail = entry;

    slotp->event_log.data_total += length;
    slotp->event_log.data_start =
        (start + length) % SDD_DEBUG_DATA_BUFFER_LEN;

    while (slotp->event_log.data_total > SDD_DEBUG_DATA_BUFFER_LEN) {
        shrink_data_list(slot);
    }
}

#else  /* SDD_DEBUG_DATA_BUFFER_LEN == 0 */
#  define init_data_list(s)
#  define deinit_data_list(s)
#  define shrink_data_list(s)
#  define add_data(s, e, d, l)
#endif

void sdio_event_log_init(struct sdio_slot *slot)
{
    struct sdio_slot_priv *slotp = slot->priv;

    os_spinlock_init(&slotp->event_log.lock);

    slotp->event_log.entries = os_alloc_big(sizeof(struct sdio_event_entry)
                                            * SDD_DEBUG_EVENT_LOG_LEN);
    if (slotp->event_log.entries == NULL) {
        slot_warning(slot, "event log unavailable");
        return;
    }

    slotp->event_log.head = slotp->event_log.tail = 0;
    slotp->event_log.seq_num = 0;

    init_data_list(slot);
    sdio_os_event_log_init(slot);
}

void sdio_event_log_deinit(struct sdio_slot *slot)
{
    struct sdio_slot_priv *slotp = slot->priv;

    sdio_os_event_log_deinit(slot);
    deinit_data_list(slot);

    os_free_big(slotp->event_log.entries);
    os_spinlock_destroy(&slotp->event_log.lock);
}

static void add_event(struct sdio_slot *slot, enum sdio_event_type type)
{
    struct sdio_slot_priv *slotp = slot->priv;
    struct sdio_event_entry *e;

    e = slotp->event_log.entries + slotp->event_log.head;

    e->time_ms = os_current_time_ms();
    e->seq_num = slotp->event_log.seq_num++;
    e->type = type;

    slotp->event_log.head = (slotp->event_log.head + 1) % SDD_DEBUG_EVENT_LOG_LEN;
    if (slotp->event_log.head == slotp->event_log.tail) {
        slotp->event_log.tail = (slotp->event_log.tail + 1) % SDD_DEBUG_EVENT_LOG_LEN;
    }
}

void sdio_event_log(struct sdio_slot *slot, enum sdio_event_type type, ...)
{
    struct sdio_slot_priv *slotp = slot->priv;
    va_list args;
    os_int_status_t istate;
    const struct sdio_cmd *cmd;
    struct sdio_event_entry *e;

    va_start(args, type);

    if (slotp->event_log.entries == NULL) {
        return;
    }

    os_spinlock_lock_intsave(&slotp->event_log.lock, &istate);

    e = slotp->event_log.entries + slotp->event_log.head;

#if SDD_DEBUG_DATA_BUFFER_LEN > 0
    /* If we're about to overwrite an log entry for an event that has
     * data, remove the data for the entry from the data list. */
    if (slotp->event_log.data_head == e) {
        shrink_data_list(slot);
    }
#endif

    switch (type) {
    case SDD_EVENT_CMD:
        cmd = va_arg(args, const struct sdio_cmd *);
        e->cmd.flags = cmd->flags;
        if (cmd->flags & SDD_CMD_FLAG_CSPI) {
            e->cmd.cspi.cmd  = cmd->cspi.cmd;
            e->cmd.cspi.addr = cmd->cspi.addr;
            if (cmd->cspi.cmd & CSPI_BURST) {
                e->cmd.cspi.val = cmd->len;
            } else {
                e->cmd.cspi.val = cmd->cspi.val;
            }
        } else {
            e->cmd.sdio.cmd = cmd->sdio.cmd;
            e->cmd.sdio.arg = cmd->sdio.arg;
        }
        e->cmd.status = 0xff;
#if SDD_DEBUG_DATA_BUFFER_LEN > 0
        e->cmd.data.length = 0;
#endif
        break;
    case SDD_EVENT_CARD_INT:
        break;
    case SDD_EVENT_INT_UNMASKED:
    case SDD_EVENT_INT_MASKED:
        e->functions = va_arg(args, unsigned);
        break;
    case SDD_EVENT_POWER_ON:
    case SDD_EVENT_POWER_OFF:
    case SDD_EVENT_RESET:
        break;
    case SDD_EVENT_CLOCK_FREQ:
        e->val = va_arg(args, int);
        break;
    }
    add_event(slot, type);

    os_spinlock_unlock_intrestore(&slotp->event_log.lock, &istate);

    va_end(args);
}

void sdio_event_log_set_response(struct sdio_slot *slot, const struct sdio_cmd *cmd)
{
    struct sdio_slot_priv *slotp = slot->priv;
    os_int_status_t istate;
    unsigned evt;
    struct sdio_event_entry *e;

    if (slotp->event_log.entries == NULL) {
        return;
    }

    os_spinlock_lock_intsave(&slotp->event_log.lock, &istate);

    /* Find latest cmd event without a response. */
    evt = slotp->event_log.head;
    do {
        evt = (evt + (SDD_DEBUG_EVENT_LOG_LEN - 1)) % SDD_DEBUG_EVENT_LOG_LEN;
    } while (slotp->event_log.entries[evt].type != SDD_EVENT_CMD);

    e = slotp->event_log.entries + evt;

    e->cmd.status = cmd->status;
    if (cmd->flags & SDD_CMD_FLAG_CSPI) {
        if (!(cmd->cspi.cmd & CSPI_BURST)) {
            e->cmd.cspi.val = cmd->cspi.val;
        }
        e->cmd.cspi.response = cmd->cspi.response;
    } else {
        e->cmd.sdio.response = cmd->sdio.response;
    }

    if (cmd->data) {
        add_data(slot, e, cmd->data, cmd->len);
    }

    os_spinlock_unlock_intrestore(&slotp->event_log.lock, &istate);
}

static unsigned event_log_len(struct sdio_slot *slot)
{
    struct sdio_slot_priv *slotp = slot->priv;

    return ((slotp->event_log.head - slotp->event_log.tail) + SDD_DEBUG_EVENT_LOG_LEN)
        % SDD_DEBUG_EVENT_LOG_LEN;
}

struct sdio_event_entry *sdio_event_log_get_entry(struct sdio_slot *slot, unsigned n)
{
    struct sdio_slot_priv *slotp = slot->priv;
    os_int_status_t istate;
    struct sdio_event_entry *e = NULL;

    os_spinlock_lock_intsave(&slotp->event_log.lock, &istate);

    if (n < event_log_len(slot)) {
        e = slotp->event_log.entries + (slotp->event_log.tail + n) % SDD_DEBUG_EVENT_LOG_LEN;
    }

    os_spinlock_unlock_intrestore(&slotp->event_log.lock, &istate);

    return e;
}

#endif /* #ifdef SDD_DEBUG_EVENT_LOG */
