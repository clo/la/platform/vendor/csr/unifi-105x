/*
 * CSPI mode control.
 *
 * Copyright (C) 2007 Cambridge Silicon Radio Ltd.
 *
 * Refer to LICENSE.txt included with this source code for details on
 * the license terms.
 */
#include <sdioemb/cspi.h>
#include <sdioemb/sdio_csr.h>

#include "sdio_layer.h"
#include "sdio_config.h"

/**
 * Map of card manufacture and card IDs to the generic function and
 * address of the SDIO_MODE register.
 *
 * New chips that support CSPI should be added to this table.
 */
static const struct sdio_mode_map {
    uint16_t manf_id;
    uint16_t card_id;
    int      gen_func;
    uint32_t addr;
} sdio_mode[] = {
    {
        SDIO_MANF_ID_CSR,
        SDIO_CARD_ID_CSR_BC6,
        2,
        0xfc26 << 1,
    },
    {
        SDIO_MANF_ID_CSR,
        SDIO_CARD_ID_CSR_BC7,
        2,
        0xf935 << 1,
    },
    {
        SDIO_MANF_ID_CSR,
        SDIO_CARD_ID_CSR_DASH,
        2,
        0xf935 << 1,
    },
    {
        SDIO_MANF_ID_CSR,
        SDIO_CARD_ID_CSR_CINDERELLA,
        1,
        0xf935 << 1,
    },
};

#define NUM_CSPI_CARDS (sizeof(sdio_mode)/sizeof(sdio_mode[0]))

static struct sdio_card_io_ops cspi_io_ops = {
    cspi_io_read8,
    cspi_io_write8,
    cspi_io_read,
    cspi_io_write,
};

/**
 * Determine if CSPI is enabled on a card.
 *
 * A CSPI write to the CSPI_PADDING register with default values is
 * attempted. (Can't do a read as the padding values are not known.)
 *
 * @param slot the slot for the card.
 *
 * @return true if CSPI is enabled; false otherwise.
 */
int cspi_is_enabled(struct sdio_slot *slot)
{
    uint16_t cspi_padding;

    slot->cspi_reg_pad   = CSPI_PADDING_REG_DFLT;
    slot->cspi_burst_pad = CSPI_PADDING_BURST_DFLT;

    cspi_padding = CSPI_PADDING_REG(slot->cspi_reg_pad)
                   | CSPI_PADDING_BURST(slot->cspi_burst_pad);

    return cspi_raw_word_cmd(slot, CSPI_WRITE | CSPI_FUNC(0), CSPI_PADDING, &cspi_padding) == 0;
}

/**
 * Count the number of functions when in CSPI mode.
 *
 * The number of functions is determined by the settable bits in the
 * CCCR Interrupt Enable register.
 *
 * @param slot the slot for the card.
 *
 * @return number of functions in the card; -ve on I/O error.
 */
static int cspi_num_functions(struct sdio_slot *slot)
{
    int r, n = 0;
    uint16_t int_en = 0xfe; /* leave MIE clear */

    r = cspi_raw_word_cmd(slot, CSPI_WRITE | CSPI_FUNC(0), SDIO_CCCR_INT_EN, &int_en);
    if (r) {
        return r;
    }
    r = cspi_raw_word_cmd(slot, CSPI_READ | CSPI_FUNC(0), SDIO_CCCR_INT_EN, &int_en);
    if (r) {
        return r;
    }

    while (int_en >>= 1) {
        n++;
    }
    return n;
}

/**
 * Initialize a card using the CSPI transport.
 *
 * @param slot the slot for the card.
 *
 * @return 0 on success; -ve on I/O error.
 *
 * @see sdio_card_init
 */
int cspi_card_init(struct sdio_slot *slot)
{
    struct sdio_slot_priv *slotp = slot->priv;
    int n;

    n = cspi_num_functions(slot);
    if (n < 0) {
        return n;
    }
    slotp->num_functions = n;

    slotp->io_ops = cspi_io_ops;

    return 0;
}

/**
 * Enable and configure CPSI mode by writing the SDIO_MODE,
 * CSPI_PADDING and CSPI_MODE registers.
 *
 * @param sdev the SDIO device for the generic function.
 * @param addr byte address of the SDIO_MODE register.
 * @param val  value to write to SDIO_MODE.
 *
 * @return 0 on success.
 * @return -EIO, -ETIMEDOUT if I/O error occured while writing to card.
 */
static int switch_to_cspi(struct sdio_dev *sdev, uint32_t addr, uint8_t val)
{
    struct sdio_dev_priv *sdevp = sdev->priv;
    struct sdio_slot *slot = sdevp->slot;
    struct sdio_slot_priv *slotp = slot->priv;
    int ret;

    slot->cspi_reg_pad = SDD_CSPI_REG_PADDING;
    slot->cspi_burst_pad = SDD_CSPI_BURST_PADDING;

    /* Ignore errors as the mode switch may occur before the SDIO-SPI
     * response is generated. */
    sdio_io_write8(sdev, sdev->function, addr, val);

    ret = cspi_io_write8(sdev, 0, CSPI_PADDING,
                         CSPI_PADDING_REG(slot->cspi_reg_pad)
                         | CSPI_PADDING_BURST(slot->cspi_burst_pad));
    if (ret) {
        return ret;
    }
    ret = cspi_io_write8(sdev, 0, CSPI_MODE, slot->caps.cspi_mode);
    if (ret) {
        return ret;
    }

    slotp->io_ops = cspi_io_ops;

    return 0;
}

/**
 * Enable CSPI mode.
 *
 * If the mode switch is successful, the slot's I/O operations will be
 * set to CSPI operations.
 *
 * In the unlikely event of the mode switch being unsuccessful, the
 * mode of the card (CSPI or still SDIO-SPI) is \e undefined.  The
 * mode switch may be retried by calling cspi_enable() again.
 *
 * @param slot the slot to enable CSPI on.
 *
 * @return 0 on success.
 * @return -EINVAL if CSPI mode is not supported by the card.
 * @return -IO, -ETIMEDOUT if an I/O error occured.
 */
int cspi_enable(struct sdio_slot *slot)
{
    struct sdio_slot_priv *slotp = slot->priv;
    struct sdio_dev *f0 = slotp->functions[0];
    struct sdio_dev *fdev = NULL;
    uint32_t addr = 0;
    int i, ret;

    /* Find generic function and SDIO_MODE address. */
    for (i = 0; i < NUM_CSPI_CARDS; i++) {
        if (sdio_mode[i].manf_id == f0->vendor_id
            && sdio_mode[i].card_id == f0->device_id)
        {
            fdev = slotp->functions[sdio_mode[i].gen_func];
            addr = sdio_mode[i].addr;
            break;
        }
    }
    if (fdev == NULL) {
        return -EINVAL;
    }

    ret = sdio_enable_function(fdev);
    if (ret) {
        return ret;
    }

    return switch_to_cspi(fdev, addr, SDIO_MODE_CSPI_EN);
}
