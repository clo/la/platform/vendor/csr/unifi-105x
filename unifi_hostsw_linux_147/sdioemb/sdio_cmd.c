/*
 * Command queuing and dispatch.
 *
 * Copyright (C) 2007 Cambridge Silicon Radio Ltd.
 *
 * Refer to LICENSE.txt included with this source code for details on
 * the license terms.
 */
#include "sdio_layer.h"

static int queue_cmd(struct sdio_slot *slot, struct sdio_cmd *cmd)
{
    struct sdio_dev *fdev = cmd->owner;
    struct sdio_slot_priv *slotp = slot->priv;
    struct sdio_dev_priv *fdevp = fdev->priv;
    os_int_status_t istate;
    int ret = 0;

    os_spinlock_lock_intsave(&slotp->lock, &istate);

    if (fdevp->queued_cmd == NULL) {
        fdevp->queued_cmd = cmd;
    } else {
        ret = -EAGAIN;
    }

    os_spinlock_unlock_intrestore(&slotp->lock, &istate);

    return ret;
}


static void dequeue_cmd(struct sdio_slot *slot, struct sdio_cmd *cmd)
{
    struct sdio_dev *fdev = cmd->owner;
    struct sdio_slot_priv *slotp = slot->priv;
    struct sdio_dev_priv *fdevp = fdev->priv;
    os_int_status_t istate;

    os_spinlock_lock_intsave(&slotp->lock, &istate);

    fdevp->queued_cmd = NULL;
    slotp->busy = 0;

    os_spinlock_unlock_intrestore(&slotp->lock, &istate);
}


static void check_queues(struct sdio_slot *slot)
{
    struct sdio_slot_priv *slotp = slot->priv;
    struct sdio_cmd *cmd = NULL;
    os_int_status_t istate;

    /* round-robin all functions and submit the first command found. */
    os_spinlock_lock_intsave(&slotp->lock, &istate);

    if (!slotp->busy) {
        int f;
        struct sdio_dev *fdev;
        struct sdio_dev_priv *fdevp;

        for (f = 0; f < SDD_MAX_FUNCTIONS; f++) {
            slotp->last_function++;
            if (slotp->last_function >= SDD_MAX_FUNCTIONS) {
                slotp->last_function = 0;
            }

            fdev = slotp->functions[slotp->last_function];
            if (!fdev) {
                continue;
            }
            fdevp = fdev->priv;

            cmd = fdevp->queued_cmd;
            if (cmd) {
                slotp->busy = 1;
                slotp->active |= 1 << fdev->function;
                break;
            }
        }
    }

    os_spinlock_unlock_intrestore(&slotp->lock, &istate);
    
    if (cmd) {
        slot_start_cmd(slot, cmd);
    }
}

/**
 * Submit an SDIO command to a device.
 *
 * The command will be queued and executed when the card becomes
 * available.
 *
 * Each SDIO device has a queue with one entry. Therefore, only one
 * command per SDIO device may be submitted at a time.
 *
 * @param fdev the SDIO device submitting the command.
 * @param cmd  the command to submit.
 *
 * @return \e 0 if the command was queued.
 * @return \e -EAGAIN if the command queue is full.
 * @return \e -EINVAL if the data transfer is invalid (unsupported
 * buffer alignment or the length is not a valid byte or block mode
 * transfer).
 *
 * @see sdio_dev::io
 *
 * @ingroup fdriver
 */
int sdio_start_cmd(struct sdio_dev *fdev, struct sdio_cmd *cmd)
{
    struct sdio_dev_priv *fdevp = fdev->priv;
    struct sdio_slot *slot = fdevp->slot;
    int ret;

    /*
     * Sanity check data transfers:
     *  - length is a valid block or byte mode transfer).
     *  - some slot drivers can't handle odd-byte aligned buffers.
     */
    if (cmd->data) {
        if ((cmd->len > fdev->blocksize && cmd->len % fdev->blocksize != 0)
            || cmd->len == 0)
        {
            slot_warning(slot, "F%d: invalid length (cmd->len = %d)",
                         fdev->function, cmd->len);
            return -EINVAL;
        }
        if ((unsigned long)cmd->data & 1) {
            slot_warning(slot, "F%d: unsupported alignment (cmd->data = %p)",
                         fdev->function, cmd->data);
            return -EINVAL;
        }
    }

    cmd->owner = fdev;

    ret = queue_cmd(slot, cmd);
    if (ret) {
        return ret;
    }
    check_queues(slot);
    return 0;
}


/**
 * Notify the SDIO layer that an SDIO commmand has completed.
 *
 * Slot drivers should set the \a cmd's status and response and call
 * this when a command has completed (either successfully or not).
 *
 * Callable from: interrupt context.
 *
 * @param slot the slot which completed the command.
 * @param cmd  the completed command.
 *
 * @ingroup sdriver
 */
void sdio_cmd_complete(struct sdio_slot *slot, struct sdio_cmd *cmd)
{
    sdio_event_log_set_response(slot, cmd);

    if (cmd->status && !slot->card_present(slot)) {
        cmd->status = SDD_CMD_ERR_NO_CARD;
    }

    if (cmd->flags & SDD_CMD_FLAG_RAW) {
        cmd->callback(cmd);
    } else {
        dequeue_cmd(slot, cmd);
    
        cmd->callback(cmd);

        check_queues(slot);
    }
}
