/*
 * Linux PCI SHC slot driver.
 *
 * Copyright (C) 2007 Cambridge Silicon Radio Ltd.
 *
 * Refer to LICENSE.txt included with this source code for details on
 * the license terms.
 *
 * References:
 *   [SDHC] SD Host Controller Standard Specification v1.00.
 */
#include <linux/kernel.h>
#include <linux/interrupt.h>
#include <linux/spinlock.h>
#include <linux/pci.h>
#include <linux/kernel-compat.h>
#include <linux/delay.h>

#include <asm/io.h>

#include <sdioemb/sdio_api.h>
#include <sdioemb/slot_api.h>

#include "sdio_layer.h"
#include "slot_shc.h"

struct shc_data {
    struct pci_dev *pdev;
    void __iomem *addr;
    spinlock_t lock;
    struct timer_list lockup_timer;
    unsigned long quirks;
    unsigned base_clk;
    struct sdio_cmd *current_cmd;
    dma_addr_t dma_addr;
    uint8_t *data;
    size_t remaining;
    size_t block_size;
};

/*
 * No supported voltages in the capabilities register.
 *
 * Workaround: Assume 3.3V is supported.
 */
#define SLOT_SHC_QUIRK_NO_VOLTAGE_CAPS (1 << 0)

/*
 * Commands with an R5B (busy) response do not complete.
 *
 * Workaround: Use R5 instead. This will only work if the busy signal
 * is cleared sufficiently quickly before the next command is started.
 */
#define SLOT_SHC_QUIRK_R5B_BROKEN      (1 << 1)

/*
 * High speed mode doesn't work.
 *
 * Workaround: limit maximum bus frequency to 25 MHz.
 */
#define SLOT_SHC_QUIRK_HIGH_SPD_BROKEN (1 << 2)

/*
 * Data timeout (TIMEOUT_CTRL) uses SDCLK and not TMCLK.
 *
 * Workaround: set TIMEOUT_CTRL using SDCLK.
 */
#define SLOT_SHC_QUIRK_DATA_TIMEOUT_USES_SDCLK (1 << 3)

/*
 * Controller can only start and end DMA on dword (32 bit) aligned
 * addresses.
 *
 * Workaround: PIO is used on data transfers with a non-dword aligned
 * address or length.
 */
#define SHC_QUIRK_DMA_DWORD_ALIGNED_ONLY (1 << 4)

/* Software timeout for commands. For working around buggy controllers
   that occasionally fail to complete a command. */
#define SLOT_SHC_LOCKUP_TIMEOUT_MS 2000

static int shc_hw_reset(struct shc_data *shc, uint8_t rst_bits)
{
    int timeout = SHC_RESET_TIMEOUT_MS;

    writeb(rst_bits, shc->addr + SHC_SOFTWARE_RST);
    while (--timeout) {
        if (!(readb(shc->addr + SHC_SOFTWARE_RST) & rst_bits)) {
            break;
        }
        /* Must be mdelay not msleep, as called from interrupt context */
        mdelay(1);
    }
    if (!timeout) {
        dev_err(&shc->pdev->dev, "timed out waiting for reset\n");
        return -ETIMEDOUT;
    }

    return 0;
}

static int shc_hw_init(struct shc_data *shc)
{
    uint32_t caps;
    int ret;
    int timeout;

    ret = shc_hw_reset(shc, SHC_SOFTWARE_RST_ALL);
    if (ret) {
        return ret;
    }

    /* read and check various capabilties */
    caps = readl(shc->addr + SHC_CAPS);

    shc->base_clk = 1000000 * SHC_CAPS_TO_BASE_CLK_FREQ(caps);
    if (shc->base_clk == 0) {
        dev_err(&shc->pdev->dev, "cannot determine base SD clock frequency\n");
        return -EIO;
    }

    /* mask all interrupts */
    writel(0, shc->addr + SHC_INT_STATUS_EN);

    /* allow any unmasked interrupt to raise an interrupt */
    writel(SHC_INT_ALL, shc->addr + SHC_INT_SIGNAL_EN);

    /* enable internal clock, disable SD clock, and wait for internal
     * clock to stabilize */
    writew(SHC_CLOCK_CTRL_INT_CLK_EN, shc->addr + SHC_CLOCK_CTRL);
    timeout = SHC_INT_CLK_STABLE_TIMEOUT_MS;
    while (--timeout) {
        if (readw(shc->addr + SHC_CLOCK_CTRL) & SHC_CLOCK_CTRL_INT_CLK_STABLE) {
            break;
        }
        msleep(1);
    }
    if (!timeout) {
        dev_err(&shc->pdev->dev, "timed out waiting for internal clock to stabilize\n");
        return -ETIMEDOUT;
    }

    return 0;
}

static void shc_hw_stop(struct shc_data *shc)
{
    writew(0, shc->addr + SHC_CLOCK_CTRL);
}

static void shc_hw_set_high_speed(struct shc_data *shc, int hs)
{
    unsigned long flags;
    u8 host_ctrl;

    spin_lock_irqsave(&shc->lock, flags);

    host_ctrl = readb(shc->addr + SHC_HOST_CTRL) & ~SHC_HOST_CTRL_HIGH_SPD_EN;
    if (hs) {
        host_ctrl |= SHC_HOST_CTRL_HIGH_SPD_EN;
    }
    writeb(host_ctrl, shc->addr + SHC_HOST_CTRL);

    spin_unlock_irqrestore(&shc->lock, flags);
}

static int shc_hw_set_clock_rate(struct shc_data *shc, int clk)
{
    uint16_t clk_reg;

    /* Stop the clock before changing the frequency. */
    clk_reg = readw(shc->addr + SHC_CLOCK_CTRL);
    clk_reg &= ~SHC_CLOCK_CTRL_SD_CLK_EN;
    writew(clk_reg, shc->addr + SHC_CLOCK_CTRL);

    shc_hw_set_high_speed(shc, clk > SDIO_CLOCK_FREQ_NORMAL_SPD);

    if (clk) {
        int divisor;
        unsigned long timeout_clks;
        uint8_t timeout_ctrl;

        /* Find maximum possible bus freq <= clk (or the minimum the
           controller is capable of)... */
        for (divisor = 1; divisor < 256; divisor *= 2) {
            if (shc->base_clk / divisor <= clk) {
                break;
            }
        }
        clk = shc->base_clk / divisor;

        /* ...and start it. */
        clk_reg = SHC_CLOCK_CTRL_DIV(divisor) | SHC_CLOCK_CTRL_INT_CLK_EN;
        writew(clk_reg, shc->addr + SHC_CLOCK_CTRL);
        clk_reg |= SHC_CLOCK_CTRL_SD_CLK_EN;
        writew(clk_reg, shc->addr + SHC_CLOCK_CTRL);

        /*
         * Set the data timeout to 1 s.
         */
        if (shc->quirks & SLOT_SHC_QUIRK_DATA_TIMEOUT_USES_SDCLK) {
            timeout_clks = clk;
        } else {
            timeout_clks = shc->base_clk;
        }
        for (timeout_ctrl = 0; timeout_ctrl < SHC_TIMEOUT_CTRL_MAX; timeout_ctrl++) {
            if ((1 << (timeout_ctrl + 13)) >= timeout_clks) {
                break;
            }
        }
        writeb(timeout_ctrl, shc->addr + SHC_TIMEOUT_CTRL);
    }

    return clk;
}

static void shc_hw_set_bus_width(struct shc_data *shc, int bus_width)
{
    unsigned long flags;
    u8 host_ctrl;

    spin_lock_irqsave(&shc->lock, flags);

    host_ctrl = readb(shc->addr + SHC_HOST_CTRL);
    if (bus_width == 4) {
        host_ctrl |= SHC_HOST_CTRL_4BIT;
    } else {
        host_ctrl &= ~SHC_HOST_CTRL_4BIT;
    }
    writeb(host_ctrl, shc->addr + SHC_HOST_CTRL);

    spin_unlock_irqrestore(&shc->lock, flags);
}

/*
 * Interrupts are masked/unmasked using INT_STATUS_EN (not
 * INT_SIGNAL_EN) as this is what the card interrupt requires.
 */
static void shc_hw_enable_int_signal(struct shc_data *shc, uint32_t sigs)
{
    unsigned long flags;
    uint32_t stat_en;

    spin_lock_irqsave(&shc->lock, flags);

    stat_en = readl(shc->addr + SHC_INT_STATUS_EN);
    writel(stat_en | sigs, shc->addr + SHC_INT_STATUS_EN);

    (void)readl(shc->addr + SHC_INT_STATUS_EN); /* flush posted writes */
    spin_unlock_irqrestore(&shc->lock, flags);
}

static void shc_hw_disable_int_signal(struct shc_data *shc, uint32_t sigs)
{
    unsigned long flags;
    uint32_t stat_en;

    spin_lock_irqsave(&shc->lock, flags);

    stat_en = readl(shc->addr + SHC_INT_STATUS_EN);
    writel(stat_en & ~sigs, shc->addr + SHC_INT_STATUS_EN);

    (void)readl(shc->addr + SHC_INT_STATUS_EN); /* flush posted writes */
    spin_unlock_irqrestore(&shc->lock, flags);
}

static void shc_hw_read_block_pio(struct shc_data *shc)
{
    size_t len;
    uint32_t dword;
    int bytes = 0;
    size_t i;

    len = min(shc->block_size, shc->remaining);

    for (i = 0; i < len; i++) {
        if (bytes == 0) {
            dword = readl(shc->addr + SHC_BUFFER_DATA_PORT);
            bytes = 4;
        }

        *(shc->data++) = dword & 0xff;
        dword >>= 8;
        shc->remaining--;
        bytes--;
    }
}

static void shc_hw_write_block_pio(struct shc_data *shc)
{
    size_t len;
    uint32_t dword = 0;
    int bytes = 0;
    size_t i;

    len = min(shc->block_size, shc->remaining);

    for (i = 0; i < len; i++) {
        dword |= ((uint32_t)*(shc->data++)) << (bytes * 8);
        shc->remaining--;
        bytes++;

        if (bytes == 4 || shc->remaining == 0) {
            writel(dword, shc->addr + SHC_BUFFER_DATA_PORT);
            dword = 0;
            bytes = 0;
        }
    }
}

static void shc_hw_setup_data_pio(struct shc_data *shc)
{
    struct sdio_cmd *cmd = shc->current_cmd;

    shc->dma_addr = 0;
    shc->data = cmd->data;
    shc->remaining = cmd->len;
    shc->block_size = cmd->owner->blocksize;

    if (cmd->flags & SDD_CMD_FLAG_READ) {
        shc_hw_enable_int_signal(shc, SHC_INT_RD_BUF_RDY);
    } else {
        shc_hw_enable_int_signal(shc, SHC_INT_WR_BUF_RDY);
    }
}

static int int_stat_to_status(uint32_t int_stat)
{
    int status;

    if (int_stat & (SHC_INT_ERR_CMD_ALL | SHC_INT_ERR_DAT_ALL)) {
        if (int_stat & SHC_INT_ERR_CMD_ALL) {
            status = SDD_CMD_ERR_CMD;
        } else {
            status = SDD_CMD_ERR_DAT;
        }
        if (int_stat & (SHC_INT_ERR_CMD_TIMEOUT | SHC_INT_ERR_DAT_TIMEOUT)) {
            status |= SDD_CMD_ERR_TIMEOUT;
        } else {
            status |= SDD_CMD_ERR_CRC;
        }
    } else {
        status = SDD_CMD_OK;
    }
    return status;
}

static void shc_cmd_complete(struct sdio_slot *slot, int status)
{
    struct shc_data *shc = slot->drv_data;
    struct sdio_cmd *cmd = shc->current_cmd;
    unsigned long flags;

    spin_lock_irqsave(&shc->lock, flags);
    cmd = shc->current_cmd;
    shc->current_cmd = NULL;
    spin_unlock_irqrestore(&shc->lock, flags);

    if (cmd == NULL) {
        dev_err(&shc->pdev->dev, "spurious command complete interrupt\n");
        return;
    }

    cmd->status = status;

    /* disable transfer related interrupts */
    shc_hw_disable_int_signal(shc, SHC_INT_ERR_ALL | SHC_INT_CMD_COMPLETE
                              | SHC_INT_TRANSFER_COMPLETE
                              | SHC_INT_RD_BUF_RDY | SHC_INT_WR_BUF_RDY);

    del_timer(&shc->lockup_timer);

    /* Read response if it's valid. */
    if (!(cmd->status & SDD_CMD_ERR_CMD)) {
        switch (cmd->flags & SDD_CMD_FLAG_RESP_MASK) {
        case SDD_CMD_FLAG_RESP_NONE:
            break;
        case SDD_CMD_FLAG_RESP_R1:
        case SDD_CMD_FLAG_RESP_R1B:
        case SDD_CMD_FLAG_RESP_R4:
        case SDD_CMD_FLAG_RESP_R5:
        case SDD_CMD_FLAG_RESP_R5B:
        case SDD_CMD_FLAG_RESP_R6:
            cmd->sdio.response.r1 = readl(shc->addr + SHC_RESPONSE_0_31);
            break;
        default:
            dev_err(&shc->pdev->dev, "response format not supported\n");
        }
    }

    if (cmd->data) {
        pci_unmap_single(shc->pdev, shc->dma_addr, cmd->len,
                         (cmd->flags & SDD_CMD_FLAG_READ) ? DMA_FROM_DEVICE : DMA_TO_DEVICE);
    }

    /*
     * After an error or abort, reset the DAT and CMD line state
     * machines.  See [SDHC] section 3.7.
     */
    if (cmd->status != SDD_CMD_OK || (cmd->flags & SDD_CMD_FLAG_ABORT)) {
        /* Some controllers do not like having both blocks reset at the same time */
        shc_hw_reset(shc, SHC_SOFTWARE_RST_CMD);
        shc_hw_reset(shc, SHC_SOFTWARE_RST_DAT);
    }

    sdio_cmd_complete(slot, cmd);
}

static void shc_lockup_timer_fn(unsigned long data)
{
    struct sdio_slot *slot = (struct sdio_slot *)data;
    struct shc_data *shc = slot->drv_data;
    uint32_t int_stat;
    int status;

    int_stat = readl(shc->addr + SHC_INT_STATUS);
    status = int_stat_to_status(int_stat);
    if (status == SDD_CMD_OK) {
        if (int_stat & SHC_INT_CMD_COMPLETE) {
            status = SDD_CMD_ERR_DAT_OTHER;
        } else {
            status = SDD_CMD_ERR_CMD_OTHER;
        }
    }
    shc_cmd_complete(slot, status);
}

static irqreturn_t shc_int_handler(int irq, void *dev_id)
{
    struct sdio_slot *slot = dev_id;
    struct shc_data *shc = slot->drv_data;
    uint32_t int_stat;

    int_stat = readl(shc->addr + SHC_INT_STATUS);
    if (int_stat == 0) {
        return IRQ_NONE;
    }
    /* Clear interrupt status. */
    writel(int_stat, shc->addr + SHC_INT_STATUS);

    if (int_stat & SHC_INT_RD_BUF_RDY) {
        shc_hw_read_block_pio(shc);
    }
    if (int_stat & SHC_INT_WR_BUF_RDY) {
        shc_hw_write_block_pio(shc);
    }

    if (int_stat & (SHC_INT_ERR_ALL | SHC_INT_CMD_COMPLETE | SHC_INT_TRANSFER_COMPLETE)) {
        shc_cmd_complete(slot, int_stat_to_status(int_stat));
    }

    if (int_stat & SHC_INT_CARD_INT) {
        sdio_interrupt(slot);
    }

    return IRQ_HANDLED;
}

static int shc_set_bus_freq(struct sdio_slot *slot, int clk)
{
    struct shc_data *shc = slot->drv_data;
    
    if (clk == SDD_BUS_FREQ_IDLE) {
        clk = 0;
    }
    return shc_hw_set_clock_rate(shc, clk);
}

static int shc_set_bus_width(struct sdio_slot *slot, int bus_width)
{
    struct shc_data *shc = slot->drv_data;

    shc_hw_set_bus_width(shc, bus_width);
    return 0;
}

static int shc_start_cmd(struct sdio_slot *slot, struct sdio_cmd *cmd)
{
    struct shc_data *shc = slot->drv_data;
    uint32_t system_addr;
    uint16_t block_size, block_count;
    uint16_t transfer_mode, command;

    static const uint16_t resp_map[] = {
        [SDD_CMD_FLAG_RESP_NONE] = 0,
        [SDD_CMD_FLAG_RESP_R1]   = SHC_CMD_RESP_48  | SHC_CMD_RESP_IDX_CHK | SHC_CMD_RESP_CRC_CHK,
        [SDD_CMD_FLAG_RESP_R1B]  = SHC_CMD_RESP_48B | SHC_CMD_RESP_IDX_CHK | SHC_CMD_RESP_CRC_CHK,
        [SDD_CMD_FLAG_RESP_R2]   = SHC_CMD_RESP_136 | SHC_CMD_RESP_CRC_CHK,
        [SDD_CMD_FLAG_RESP_R3]   = SHC_CMD_RESP_48,
        [SDD_CMD_FLAG_RESP_R4]   = SHC_CMD_RESP_48,
        [SDD_CMD_FLAG_RESP_R5]   = SHC_CMD_RESP_48  | SHC_CMD_RESP_IDX_CHK | SHC_CMD_RESP_CRC_CHK,
        [SDD_CMD_FLAG_RESP_R5B]  = SHC_CMD_RESP_48B | SHC_CMD_RESP_IDX_CHK | SHC_CMD_RESP_CRC_CHK,
        [SDD_CMD_FLAG_RESP_R6]   = SHC_CMD_RESP_48  | SHC_CMD_RESP_IDX_CHK | SHC_CMD_RESP_CRC_CHK,
    };

    shc->current_cmd = cmd;

    /* The first command (which will always be a CMD0) must be
     * preceeded by at least 64 clocks. */
    if (cmd->sdio.cmd == 0) {
        udelay(64 * 1000000 / slot->clock_freq + 1);
    }

    /* enable command complete, error, data transfer
     * etc. interrupts */
    shc_hw_enable_int_signal(shc, SHC_INT_ERR_CMD_ALL | SHC_INT_ERR_DAT_ALL
                             | (cmd->data ? SHC_INT_TRANSFER_COMPLETE : SHC_INT_CMD_COMPLETE));

    /* Set up data for DMA, poke registers to start cmd */
    transfer_mode = 0;
    if (cmd->data) {
        bool use_pio = shc->quirks & SHC_QUIRK_DMA_DWORD_ALIGNED_ONLY
            && ((unsigned long)cmd->data & 0x3 || cmd->len & 0x3);

        if (use_pio) {
            shc_hw_setup_data_pio(shc);
        } else {
            shc->dma_addr = pci_map_single(shc->pdev, cmd->data, cmd->len,
                                           (cmd->flags & SDD_CMD_FLAG_READ) ? DMA_FROM_DEVICE
                                           : DMA_TO_DEVICE);
            transfer_mode |= SHC_TRANSFER_MODE_DMA_EN;
        }
        if (cmd->len < cmd->owner->blocksize) {
            block_size = cmd->len;
            block_count = 1;
        } else {
            block_size = cmd->owner->blocksize;
            block_count = cmd->len / block_size;
        }
        system_addr = shc->dma_addr;
    } else {
        block_size = block_count = system_addr = 0;
    }

    if (cmd->flags & SDD_CMD_FLAG_READ) {
        transfer_mode |= SHC_TRANSFER_MODE_DATA_READ;
    }
    if (block_count > 0) {
        transfer_mode |= SHC_TRANSFER_MODE_MULTI_BLK | SHC_TRANSFER_MODE_BLK_CNT_EN;
    }

    command = SHC_CMD_IDX(cmd->sdio.cmd) | resp_map[cmd->flags & SDD_CMD_FLAG_RESP_MASK];
    if (cmd->data) {
        command |= SHC_CMD_DATA_PRESENT;
        if (shc->quirks & SLOT_SHC_QUIRK_R5B_BROKEN) {
            /* Swap R5B for R5 */
            if ((command & SHC_CMD_RESP_48B) == SHC_CMD_RESP_48B) {
                command &= ~(SHC_CMD_RESP_48B);
                command |= SHC_CMD_RESP_48;
            }
        }
    }
    if (cmd->flags & SDD_CMD_FLAG_ABORT) {
        command |= SHC_CMD_TYPE_ABORT;
    }

    if (system_addr)
        writel(system_addr, shc->addr + SHC_SYSTEM_ADDRESS);
    if (block_count > 0)
        writew(block_size, shc->addr + SHC_BLOCK_SIZE);
    if (block_count > 0)
        writew(block_count, shc->addr + SHC_BLOCK_COUNT);
    writel(cmd->sdio.arg, shc->addr + SHC_ARG);
    writew(transfer_mode, shc->addr + SHC_TRANSFER_MODE);

    mod_timer(&shc->lockup_timer, jiffies + msecs_to_jiffies(SLOT_SHC_LOCKUP_TIMEOUT_MS));

    (void)readl(shc->addr); /* flush posted writes */
    writew(command, shc->addr + SHC_CMD);

    return 0;
}

static int shc_card_present(struct sdio_slot *slot)
{
    struct shc_data *shc = slot->drv_data;
    uint32_t state;

    /* read card present register */
    state = readl(shc->addr + SHC_PRESENT_STATE);

    return state & SHC_PRESENT_STATE_CARD_PRESENT;
}

static int shc_card_power(struct sdio_slot *slot, enum sdio_power power)
{
    struct shc_data *shc = slot->drv_data;
    uint32_t caps;
    uint8_t pwr_ctrl;

    caps = readl(shc->addr + SHC_CAPS);

    if( shc->quirks & SLOT_SHC_QUIRK_NO_VOLTAGE_CAPS )
    {
        /* Add in 3.3V to capabilites - the hardware supports this, but doesn't say! */
        caps |= SHC_CAPS_PWR_3V3;
    }
    
    switch (power) {
    case SDIO_POWER_OFF:
        pwr_ctrl = 0;
        break;
    case SDIO_POWER_3V3:
        if (caps & SHC_CAPS_PWR_3V3) {
            pwr_ctrl = SHC_PWR_CTRL_3V3;
            writeb(pwr_ctrl, shc->addr + SHC_PWR_CTRL);
            pwr_ctrl |= SHC_PWR_CTRL_ON;
        } else {
            return -ENOTSUPP;
        }
        break;
    default:
        return -EINVAL;
    }

    writeb(pwr_ctrl, shc->addr + SHC_PWR_CTRL);

    return 0;
}

static void shc_enable_card_int(struct sdio_slot *slot)
{
    struct shc_data *shc = slot->drv_data;

    shc_hw_enable_int_signal(shc, SHC_INT_CARD_INT);
}

static void shc_disable_card_int(struct sdio_slot *slot)
{
    struct shc_data *shc = slot->drv_data;

    shc_hw_disable_int_signal(shc, SHC_INT_CARD_INT);
}


static int shc_hard_reset(struct sdio_slot *slot)
{
    struct shc_data *shc = slot->drv_data;
    struct sdio_slot_priv *slotp = slot->priv;
    uint8_t pwr_ctrl;

    shc_hw_set_clock_rate(shc, SDD_BUS_FREQ_OFF);

    /* Power cycle the card. */
    pwr_ctrl = readb(shc->addr + SHC_PWR_CTRL);
    writeb(0, shc->addr + SHC_PWR_CTRL);
    msleep(100); /* short delay to allow voltage to drop */
    writeb(pwr_ctrl, shc->addr + SHC_PWR_CTRL);

    shc_hw_set_clock_rate(shc, slotp->current_clock_freq);

    return 0;
}

static struct sdio_slot *shc_pci_slot_init(struct pci_dev *pci, int slot_num, unsigned long quirks)
{
    struct sdio_slot *slot;
    struct shc_data *shc;
    int ret;

    slot = sdio_slot_alloc(sizeof(struct shc_data));
    if (!slot) {
        goto err_alloc;
    }
    shc = slot->drv_data;

    shc->pdev = pci;
    shc->addr = pci_iomap(pci, slot_num, pci_resource_len(pci, slot_num));
    if (!shc->addr) {
        goto err_iomap;
    }
    shc->quirks = quirks;

    printk("Registered slot %s/%d  caps %08x quirks %lx\n", pci->dev.bus_id, slot_num,
           readl(shc->addr + SHC_CAPS), quirks);

    if (shc_hw_init(shc) != 0) {
        goto err_init;
    }

    sprintf(slot->name, "%s-%d", pci->dev.bus_id, slot_num);
    slot->set_bus_freq     = shc_set_bus_freq;
    slot->set_bus_width    = shc_set_bus_width;
    slot->start_cmd        = shc_start_cmd;
    slot->card_present     = shc_card_present;
    slot->card_power       = shc_card_power;
    slot->enable_card_int  = shc_enable_card_int;
    slot->disable_card_int = shc_disable_card_int;
    slot->hard_reset       = shc_hard_reset;

    slot->caps.max_bus_freq = shc->base_clk;
    slot->caps.max_bus_width = 4;
    if (shc->quirks & SLOT_SHC_QUIRK_HIGH_SPD_BROKEN
        && slot->caps.max_bus_freq > SDIO_CLOCK_FREQ_NORMAL_SPD) {
        slot->caps.max_bus_freq = SDIO_CLOCK_FREQ_NORMAL_SPD;
    }
    
    spin_lock_init(&shc->lock);
    init_timer (&shc->lockup_timer);
    shc->lockup_timer.data = (unsigned long)slot;
    shc->lockup_timer.function = shc_lockup_timer_fn;

    ret = request_irq(pci->irq, shc_int_handler, IRQF_SHARED, "slot_shc", slot);
    if (ret) {
        goto err_irq;
    }

    ret = sdio_slot_register(slot);
    if (ret) {
        goto err_register;
    }

    return slot;

  err_register:
    free_irq(pci->irq, slot);
  err_irq:
  err_init:
    pci_iounmap(pci, shc->addr);
  err_iomap:
    sdio_slot_free(slot);
  err_alloc:
    return NULL;
}

static void shc_pci_slot_cleanup(struct sdio_slot *slot)
{
    struct shc_data *shc = slot->drv_data;
    struct pci_dev *pci = shc->pdev;

    sdio_slot_unregister(slot);
    shc_hw_stop(shc);
    free_irq(pci->irq, slot);
    pci_iounmap(pci, shc->addr);
    sdio_slot_free(slot);
}

static int shc_pci_probe(struct pci_dev *pci, const struct pci_device_id *id)
{
    int ret;
    uint8_t num_slots, s;
    struct sdio_slot **slots;
    unsigned long quirks;

    ret = pci_enable_device(pci);
    if (ret)
        goto err_enable;
    
    ret = pci_set_dma_mask(pci, DMA_32BIT_MASK);
    if (ret) {
        goto err_set_dma_mask;
    }
    pci_set_master(pci);

    pci_read_config_byte(pci, PCI_SHC_SLOT_INFO, &num_slots);
    num_slots = ((num_slots >> 4) & 0x07) + 1;

    slots = kzalloc(sizeof(struct sdio_driver_slot *) * (num_slots + 1), GFP_KERNEL);
    if (!slots) {
        ret = -ENOMEM;
        goto err_no_slots_mem;
    }
    
    quirks = id->driver_data;
    
    pci_set_drvdata(pci, slots);

    for (s = 0; s < num_slots; s++) {
        struct sdio_slot *slot;

        slot = shc_pci_slot_init(pci, s, quirks);
        if (!slot) {
            goto err_init;
        }
        slots[s] = slot;
    }

    return 0;

  err_init:
    for (s = 0; s < num_slots; s++) {
        if (slots[s]) {
            shc_pci_slot_cleanup(slots[s]);
        }
    }
    kfree(slots);
    pci_set_drvdata(pci, NULL);
  err_set_dma_mask:
  err_no_slots_mem:
    pci_disable_device(pci);
  err_enable:

    return ret;
}

static void shc_pci_remove(struct pci_dev *pci)
{
    struct sdio_slot **slots = pci_get_drvdata(pci);
    struct sdio_slot **s;

    if (slots) {
        s = slots;
        while (*s) {
            shc_pci_slot_cleanup(*s);
            s++;
        }

        kfree(slots);
    }
}

static int shc_pci_suspend(struct pci_dev *pci, pm_message_t state)
{
    struct sdio_slot **slots = pci_get_drvdata(pci);
    struct sdio_slot **s;

    if (slots) {
        s = slots;
        while (*s) {
            /* Pass the event to the SDIO driver. */
            sdio_suspend(*s);
            s++;
        }
    }

    return 0;
}

static int shc_pci_resume(struct pci_dev *pci)
{
    int ret;
    struct sdio_slot **slots = pci_get_drvdata(pci);
    struct sdio_slot **s;

    ret = 0;
    if (slots) {
        s = slots;
        while (*s) {
            struct shc_data *shc = (*s)->drv_data;
            /* Re-initialise h/w. */
            ret = shc_hw_init(shc);
            if (ret != 0) {
                break;
            }
            /* Pass the event to the SDIO driver. */
            sdio_resume(*s);
            s++;
        }
    }

    return ret;
}


#ifndef PCI_CLASS_SYSTEM_SDHCI
#define PCI_CLASS_SYSTEM_SDHCI 0x0805
#endif

static struct pci_device_id shc_pci_id_table[] = {
    {
        .vendor      = PCI_VENDOR_ID_RICOH,
        .device      = PCI_DEVICE_ID_RICOH_R5C822,
        .subvendor   = PCI_ANY_ID,
        .subdevice   = PCI_ANY_ID,
        .driver_data = SHC_QUIRK_DMA_DWORD_ALIGNED_ONLY | SLOT_SHC_QUIRK_R5B_BROKEN,
    },
    /* The Arasan SHC card does not meet the rise time requirement for
     * high speed mode so high speed mode is very unreliable. */
    {
        PCI_DEVICE(0x1095, 0x0670),
        .driver_data = SLOT_SHC_QUIRK_HIGH_SPD_BROKEN
                       | SLOT_SHC_QUIRK_DATA_TIMEOUT_USES_SDCLK,
    },
    { PCI_DEVICE_CLASS((PCI_CLASS_SYSTEM_SDHCI << 8), 0xFFFF00) },
    { 0 },
};
MODULE_DEVICE_TABLE(pci, shc_pci_id_table);

static struct pci_driver shc_pci_driver = {
    .name     = "slot_shc",
    .id_table = shc_pci_id_table,
    .probe    = shc_pci_probe,
    .remove   = __devexit_p(shc_pci_remove),
    .suspend  = shc_pci_suspend,
    .resume   = shc_pci_resume,
};

static int __init shc_pci_init(void)
{
    return pci_register_driver(&shc_pci_driver);
}

static void __exit shc_pci_exit(void)
{
    pci_unregister_driver(&shc_pci_driver);
}

module_init(shc_pci_init);
module_exit(shc_pci_exit);

MODULE_DESCRIPTION("PCI SHC slot driver");
MODULE_AUTHOR("Cambridge Silicon Radio Ltd.");
MODULE_LICENSE("GPL and additional rights");
