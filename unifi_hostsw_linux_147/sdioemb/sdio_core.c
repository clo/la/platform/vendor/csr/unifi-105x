/*
 * SDIO device management.
 *
 * Copyright (C) 2007 Cambridge Silicon Radio Ltd.
 *
 * Refer to LICENSE.txt included with this source code for details on
 * the license terms.
 */
#include "sdio_config.h"
#include "sdio_layer.h"
#include "sdio_version.h"

/**
 * Mutex protecting card inserts/removes and driver registration.
 */
os_mutex_t sdio_core_mutex;

static struct sdio_func_driver *registered_fdrivers[SDD_FDRIVERS_MAX];

static struct sdio_dev *available_funcs[SDD_FDEVS_MAX];

/**
 * Initialize the SDIO stack.
 *
 * This must be called from the OS specific entry point of the sdio
 * module.
 */
void sdio_core_init(void)
{
    slot_info(NULL, "CSR SDIO driver (sdioemb) v%d%s %s",
             SDD_BUILDID, SDD_BUILDID_EXTRA, SDD_BUILD_DATE);

    os_mutex_init(&sdio_core_mutex);
}

static int sdio_driver_probe(struct sdio_func_driver *fdriver, struct sdio_dev *fdev)
{
    struct sdio_id_table *id;
    int ret;

    for (id = fdriver->id_table; id->vendor_id != 0; id++) {
        if ((id->vendor_id == SDD_ANY_ID || id->vendor_id == fdev->vendor_id) &&
            (id->device_id == SDD_ANY_ID || id->device_id == fdev->device_id) &&
            ((id->function == SDD_ANY_FUNC && fdev->function != SDD_UIF_FUNC)
             || id->function == fdev->function) &&
            (id->interface == SDD_ANY_IFACE || id->interface == fdev->interface))
        {
            sdio_power_on(fdev);
            sdio_set_block_size(fdev, 0);
            sdio_set_max_bus_freq(fdev, SDD_BUS_FREQ_DEFAULT);

            fdev->driver = fdriver;
            ret = fdriver->probe(fdev);
            if (ret != 0) {
                sdio_power_off(fdev);
                fdev->driver = NULL;
            }
            return ret;
        }
    }
    return -ENODEV;
}

static void sdio_driver_remove(struct sdio_func_driver *fdriver, struct sdio_dev *fdev)
{
    fdriver->remove(fdev);
    fdev->driver = NULL;
    sdio_idle_function(fdev);
    sdio_power_off(fdev);
}

/**
 * Register a function driver with the core.
 *
 * If any inserted cards that have functions that match \a fdriver's
 * list of supported functions then \a fdriver's probe() function will
 * be called for those functions.
 *
 * @param fdriver the function driver to register.
 *
 * @returns 0 on success; -ve on error:
 *   -ENOMEM - maximum number of function drivers are already registered.
 *
 * @ingroup fdriver
 */
int sdio_driver_register(struct sdio_func_driver *fdriver)
{
    int i;

    os_mutex_lock(&sdio_core_mutex);

    for (i = 0; i < SDD_FDRIVERS_MAX; i++) {
        if (registered_fdrivers[i] == NULL) {
            registered_fdrivers[i] = fdriver;
            break;
        }
    }
    if (i == SDD_FDRIVERS_MAX) {
        os_mutex_unlock(&sdio_core_mutex);
        return -ENOMEM;
    }

    slot_debug(NULL, "registered %s driver", fdriver->name);

    for (i = 0; i < SDD_FDEVS_MAX; i++) {
        if (available_funcs[i] && available_funcs[i]->driver == NULL) {
            sdio_driver_probe(fdriver, available_funcs[i]);
        }
    }

    os_mutex_unlock(&sdio_core_mutex);

    return 0;
}

/**
 * Unregister a function driver from the core.
 *
 * \a fdriver's remove() will be called for any SDIO devices currently
 * attached to the driver.
 *
 * @param fdriver the function driver to unregister.
 *
 * @ingroup fdriver
 */
void sdio_driver_unregister(struct sdio_func_driver *fdriver)
{
    int i;

    slot_debug(NULL, "unregister %s driver", fdriver->name);

    os_mutex_lock(&sdio_core_mutex);

    /* Remove any functions attached to the driver. */
    for (i = 0; i < SDD_FDEVS_MAX; i++) {
        struct sdio_dev *fdev = available_funcs[i];
        if (fdev && fdev->driver == fdriver) {
            sdio_driver_remove(fdriver, fdev);
        }
    }

    /* Remove fdriver from the registered driver list. */
    for (i = 0; i < SDD_FDRIVERS_MAX; i++) {
        if (registered_fdrivers[i] == fdriver) {
            registered_fdrivers[i] = NULL;
        }
    }

    os_mutex_unlock(&sdio_core_mutex);
}

struct sdio_dev *sdio_dev_alloc(struct sdio_slot *slot, int func)
{
    struct sdio_slot_priv *slotp = slot->priv;
    struct sdio_dev *fdev;
    struct sdio_dev_priv *fdevp;

    fdev = os_alloc(sizeof(struct sdio_dev) + sizeof(struct sdio_dev_priv));
    if (!fdev) {
        return NULL;
    }
    slotp->functions[func] = fdev;

    fdev->priv     = (struct sdio_dev_priv *)&fdev[1];
    fdev->function = func;
    fdev->slot_id  = slotp->id;
    fdev->io       = &slotp->io_ops;

    fdevp = fdev->priv;
    fdevp->slot = slot;

    return fdev;
}

void sdio_dev_free(struct sdio_dev *fdev)
{
    struct sdio_slot_priv *slotp;

    if (fdev == NULL)
        return;

    slotp = fdev->priv->slot->priv;

    slotp->functions[fdev->function] = NULL;
    os_free(fdev);
}

int sdio_dev_add(struct sdio_dev *fdev)
{
    int err;
    int i;

    err = sdio_os_dev_add(fdev);
    if (err < 0) {
        return err;
    }

    for (i = 0; i < SDD_FDEVS_MAX; i++) {
        if (available_funcs[i] == NULL) {
            available_funcs[i] = fdev;
            break;
        }
    }
    if (i == SDD_FDEVS_MAX)
        return -ENOMEM;

    for (i = 0; i < SDD_FDRIVERS_MAX; i++) {
        if (registered_fdrivers[i]) {
            sdio_driver_probe(registered_fdrivers[i], fdev);
        }
    }
    return 0;
}

void sdio_dev_del(struct sdio_dev *fdev)
{
    int i;

    if (fdev->driver) {
        sdio_driver_remove(fdev->driver, fdev);
    }

    for (i = 0; i < SDD_FDEVS_MAX; i++) {
        if (available_funcs[i] == fdev) {
            available_funcs[i] = NULL;
            break;
        }
    }

    sdio_os_dev_del(fdev);
}

static void slot_message(enum os_print_level level, struct sdio_slot *slot,
                         const char *fmt, va_list args)
{
    const char *name = NULL;

    if (slot) {
        name = slot->name ? slot->name : "(unnamed slot)";
    }
    os_vprint(level, "sdio: ", name, fmt, args);
}

void slot_error(struct sdio_slot *slot, const char *fmt, ...)
{
    va_list args;
    
    va_start(args, fmt);
    slot_message(OS_PRINT_ERROR, slot, fmt, args);
    va_end(args);
}

void slot_warning(struct sdio_slot *slot, const char *fmt, ...)
{
    va_list args;
    
    va_start(args, fmt);
    slot_message(OS_PRINT_WARNING, slot, fmt, args);
    va_end(args);
}

void slot_info(struct sdio_slot *slot, const char *fmt, ...)
{
    va_list args;
    
    va_start(args, fmt);
    slot_message(OS_PRINT_INFO, slot, fmt, args);
    va_end(args);
}

void slot_debug(struct sdio_slot *slot, const char *fmt, ...)
{
    va_list args;
    
    va_start(args, fmt);
    slot_message(OS_PRINT_DEBUG, slot, fmt, args);
    va_end(args);
}
