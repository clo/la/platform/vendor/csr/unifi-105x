/* 
 * Arcom ZEUS SDIO and SDIO-SPI/CSPI controller devices.
 *
 * Copyright (C) 2007 Cambridge Silicon Radio Ltd.
 *
 * Refer to LICENSE.txt included with this source code for details on
 * the license terms.
 */

#include <linux/init.h>
#include <linux/module.h>
#include <linux/platform_device.h>
#include <linux/spi/spi.h>

#include <asm/arch/hardware.h>
#include <asm/arch/pxa-regs.h>
#include <asm/arch/pxa2xx_spi.h>
#include <asm/arch/zeus.h>

#include "slot_pxa27x_lx.h"
#include "slot_spi_lx.h"

static int zeus_sdio_card_present(struct device *dev)
{
    return !(GPLR(ZEUS_MMC_CD_GPIO) & GPIO_bit(ZEUS_MMC_CD_GPIO));
}

static int zeus_sdio_card_power(struct device *dev, enum sdio_power power)
{
    /* Power is always enabled on the ZEUS. */
    return 0;
}

void zeus_irq_work(struct work_struct *work)
{
    struct zeus_sdio_spi_plat_data *zeus_spdata = 
        container_of(work, struct zeus_sdio_spi_plat_data, irq_work);
    struct sdio_slot *slot = container_of(zeus_spdata, struct sdio_slot, slot);
    
    sdio_interrupt(slot);
}

static int zeus_sdio_spi_enable_int(struct sdio_slot *slot)
{
    /* Check if interrupt line is LOW and in case schedule a work queue */
    if (!GPLR(98)) {
        zeus_sdio_spi_plat_data->slot = slot;
        schedule_work(&zeus_sdio_spi_plat_data->irq_work);
    }
    return 0;
}

static struct pxa27x_sdio_plat_data zeus_sdio_plat_data = {
    .card_present = zeus_sdio_card_present,
    .card_power   = zeus_sdio_card_power,
};

struct sdio_spi_plat_data zeus_sdio_spi_plat_data = {
    .enable_int = zeus_sdio_spi_enable_int,
}
/*
 * For SDIO-SPI/CSPI use the PXA270's SSP1 SPI controller. The signals
 * for this are available on the camera interface connector (J9).
 *
 * SDIO-SPI  PXA270   PXA270  
 * signal    Signal   GPIO    J9 pin
 * ---------------------------------------
 * SCLK      SSPSCLK  23      3 (CIF_MCLK)
 * CS        SSPSFRM  24      6 (CIF_FV)
 * DI        SSPTxD   25      5 (CIF_LV)
 * DO        SSPRxD   26      4 (CIF_PCLK)
 * IRQ       GPIO98   98      7 (CIF_DD0)
 */
#define ZEUS_SDIO_SPI_INT_GPIO 98

/* SSP1 SPI master. */
static struct resource pxa2xx_spi_ssp1_resources[] = {
    [0] = {
        .start = __PREG(SSCR0_P(1)),
        .end   = __PREG(SSCR0_P(1)) + 0x2c,
        .flags = IORESOURCE_MEM,
    },
    [1] = {
        .start = IRQ_SSP,
        .end   = IRQ_SSP,
        .flags = IORESOURCE_IRQ,
    },
};

static struct pxa2xx_spi_master pxa2xx_spi_ssp1_master_info = {
    .ssp_type       = PXA27x_SSP1,
    .clock_enable   = CKEN3_SSP,
    .num_chipselect = 1,
    .enable_dma     = 1,
};

/* SDIO-SPI/CSPI capable SPI device. */
static struct pxa2xx_spi_chip sdio_spi_chip_info = {
    .tx_threshold      = 8,
    .rx_threshold      = 8,
    .dma_burst_size    = 8,
    .timeout_microsecs = 64,
};

static struct spi_board_info zeus_spi_board_info = {
    .modalias        = "slot_spi",
    .max_speed_hz    = 20000000,
    .bus_num         = 1,
    .chip_select     = 0,
    .platform_data   = &zeus_sdio_spi_plat_data,
    .controller_data = &sdio_spi_chip_info,
    .irq             = IRQ_GPIO(ZEUS_SDIO_SPI_INT_GPIO),
};

static struct platform_device zeus_sdio_devices[] = {
    {
        .name          = "pxa27x-sdio",
        .id            = 0,
        .num_resources = 0,
        .dev           = {
            .platform_data = &zeus_sdio_plat_data,
        },
    },
    {
        .name          = "pxa2xx-spi",
#define SSP1_ID 1
        .id            = SSP1_ID,
        .resource      = pxa2xx_spi_ssp1_resources,
        .num_resources = ARRAY_SIZE(pxa2xx_spi_ssp1_resources),
        .dev = {
            .platform_data = &pxa2xx_spi_ssp1_master_info,
        },
    },
};

static int __init slot_zeus_init(void)
{
    int i;
    struct spi_master *master = NULL;

    /* SPI pins */
    pxa_gpio_mode(GPIO23_SCLK_MD);
    pxa_gpio_mode(GPIO24_SFRM_MD);
    pxa_gpio_mode(GPIO25_STXD_MD);
    pxa_gpio_mode(GPIO26_SRXD_MD);
    pxa_gpio_mode(ZEUS_SDIO_SPI_INT_GPIO | GPIO_IN);
    set_irq_type(IRQ_GPIO(ZEUS_SDIO_SPI_INT_GPIO), IRQT_FALLING);

    for (i = 0; i < ARRAY_SIZE(zeus_sdio_devices); i++) {
        platform_device_register(&zeus_sdio_devices[i]);
    }

    master = spi_busnum_to_master(SSP1_ID);
    if (!master) {
        printk(KERN_WARNING "slot_zeus: no SPI master\n");
        goto out;
    }
    spi_new_device(spi_busnum_to_master(SSP1_ID), &zeus_spi_board_info);
    
    INIT_WORK(&zeus_sdio_spi_plat_data->irq_work, zeus_irq_work);

  out:
    if (master)
        spi_master_put(master);
    return 0;
}

module_init(slot_zeus_init);

MODULE_DESCRIPTION("Arcom ZEUS SDIO and SDIO-SPI/CSPI slot devices.");
MODULE_AUTHOR("Cambridge Silicon Radio Ltd.");
MODULE_LICENSE("GPL and additional rights");
