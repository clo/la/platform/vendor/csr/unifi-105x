/*
 * SDIO Userspace Interface driver.
 *
 * Copyright (C) 2007 Cambridge Silicon Radio Ltd.
 *
 * Refer to LICENSE.txt included with this source code for details on
 * the license terms.
 */
#include <linux/init.h>
#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/types.h>
#include <linux/fs.h>
#include <linux/cdev.h>
#include <linux/device.h>
#include <linux/mutex.h>

#include <asm/uaccess.h>

#include <linux/kernel-compat.h>

#include <linux/sdioemb/uif.h>

#include "sdio_layer.h"

#define UIF_MAX_CARDS 16

struct sdio_uif_dev {
    struct sdio_dev *   fdev;
    int                 card_present;
    struct mutex        sdio_mutex;
    wait_queue_head_t   waitq;
    struct cdev         cdev;
    atomic_t            ref_count;

    int                 interrupt;
};

static dev_t          uif_first_dev;
static struct class * uif_class;

static void uif_get(struct sdio_uif_dev *ud)
{
    atomic_inc(&ud->ref_count);
}

static void uif_put(struct sdio_uif_dev *ud)
{
    if (atomic_dec_and_test(&ud->ref_count)) {
        kfree(ud);
    }
}


static inline int uif_num_functions(struct sdio_uif_dev *ud)
{
    /*
     * The number of functions a card has is not available to function
     * drivers so we have to peek inside the SDIO layer's private
     * data.
     */
    struct sdio_dev_priv *fdevp = ud->fdev->priv;
    struct sdio_slot_priv *slotp = fdevp->slot->priv;

    return slotp->num_functions;
}

static int uif_do_cmd(struct sdio_uif_dev *ud, struct sdio_uif_cmd *cmd, uint8_t *data)
{
    switch (cmd->type) {
    case SDD_UIF_CMD52_READ:     
        return ud->fdev->io->read8(ud->fdev, cmd->function, cmd->address, data);
    case SDD_UIF_CMD52_WRITE:
        return ud->fdev->io->write8(ud->fdev, cmd->function, cmd->address, *data);
    case SDD_UIF_CMD53_READ:
        ud->fdev->blocksize = cmd->block_size;
        return ud->fdev->io->read(ud->fdev, cmd->function, cmd->address,
                                  data, cmd->len);
    case SDD_UIF_CMD53_WRITE:
        ud->fdev->blocksize = cmd->block_size;
        return ud->fdev->io->write(ud->fdev, cmd->function, cmd->address,
                                   data, cmd->len);
    default:
        return -EINVAL;
    }
}

static int uif_wait_for_interrupt(struct sdio_uif_dev *ud)
{
    int ret = 0;

    sdio_enable_interrupt(ud->fdev);

    ret = wait_event_interruptible(ud->waitq, ud->interrupt);
    if (ret) {
        ret = -ERESTARTSYS;
    } else {
        ud->interrupt = 0;
    }

    sdio_disable_interrupt(ud->fdev);

    return ret;
}

static int uif_set_bus_width(struct sdio_uif_dev *ud, int bus_width)
{
    int ret;

    mutex_lock(&ud->sdio_mutex);
    ret = sdio_set_bus_width(ud->fdev, bus_width);
    mutex_unlock(&ud->sdio_mutex);

    return ret;
}

static int uif_reinsert(struct sdio_uif_dev *ud)
{
    struct sdio_dev_priv *fdevp = ud->fdev->priv;
    struct sdio_slot *slot = fdevp->slot;

    sdio_card_removed(slot);
    msleep(100);
    sdio_card_inserted(slot);

    return 0;
}

static int uif_set_bus_freq(struct sdio_uif_dev *ud, int bus_freq)
{
    struct sdio_dev *fdev = ud->fdev;
    struct sdio_dev_priv *fdevp = fdev->priv;
    struct sdio_slot *slot = fdevp->slot;

    if (bus_freq < 0 && bus_freq != SDD_BUS_FREQ_DEFAULT) {
        return -EINVAL;
    }

    if (bus_freq != SDD_BUS_FREQ_OFF) {
        sdio_set_max_bus_freq(ud->fdev, bus_freq);
        bus_freq = slot->clock_freq;
    }

    /* Set clock rate and start it if it isn't already running at the
       correct speed. */
    slot_set_bus_freq(slot, bus_freq);

    return 0;
}

static int uif_manf_id(struct sdio_uif_dev *ud)
{
    return ud->fdev->vendor_id;
}

static int uif_card_id(struct sdio_uif_dev *ud)
{
    return ud->fdev->device_id;
}

static int uif_get_std_if(struct sdio_uif_dev *ud, unsigned long func)
{
    struct sdio_dev_priv *fdevp = ud->fdev->priv;
    struct sdio_slot_priv *slotp = fdevp->slot->priv;

    return slotp->functions[func]->interface;
}

static int uif_get_max_block_size(struct sdio_uif_dev *ud, unsigned long func)
{
    struct sdio_dev_priv *fdevp = ud->fdev->priv;
    struct sdio_slot_priv *slotp = fdevp->slot->priv;

    return slotp->functions[func]->max_blocksize;
}

static int uif_get_block_size(struct sdio_uif_dev *ud, unsigned long func)
{
    struct sdio_dev_priv *fdevp = ud->fdev->priv;
    struct sdio_slot_priv *slotp = fdevp->slot->priv;
    int blk_sz;

    mutex_lock(&ud->sdio_mutex);
    blk_sz = slotp->functions[func]->blocksize;
    mutex_unlock(&ud->sdio_mutex);
    return blk_sz;
}

static int uif_set_block_size(struct sdio_uif_dev *ud, unsigned long blksz, unsigned long func)
{
    struct sdio_dev_priv *fdevp = ud->fdev->priv;
    struct sdio_slot_priv *slotp = fdevp->slot->priv;
    int ret;

    mutex_lock(&ud->sdio_mutex);
    ret = sdio_set_block_size(slotp->functions[func], blksz);
    mutex_unlock(&ud->sdio_mutex);
    return ret;
}

static int uif_ioctl(struct inode *inode, struct file *file, unsigned cmd, unsigned long arg)
{
    struct sdio_uif_dev *ud = file->private_data;
    struct sdio_uif_cmd scmd;
    uint8_t *data;
    int ret;

    switch (cmd) {
    case SDD_UIF_IOCQNUMFUNCS:
        return uif_num_functions(ud);
    case SDD_UIF_IOCCMD:
        if (copy_from_user(&scmd, (struct uif_cmd *)arg, sizeof(struct sdio_uif_cmd))) {
            return -EFAULT;
        }
        data = kmalloc(scmd.len, GFP_KERNEL);
        if (!data) {
            return -ENOMEM;
        }
        if (copy_from_user(data, scmd.data, scmd.len) == 0) {
            mutex_lock(&ud->sdio_mutex);
            ret = uif_do_cmd(ud, &scmd, data);
            mutex_unlock(&ud->sdio_mutex);

            if (!ret && copy_to_user(scmd.data, data, scmd.len)) {
                ret = -EFAULT;
            }
        } else {
            ret = -EFAULT;
        }
        kfree(data);
        return ret;
    case SDD_UIF_IOCWAITFORINT:
        return uif_wait_for_interrupt(ud);
    case SDD_UIF_IOCTBUSWIDTH:
        return uif_set_bus_width(ud, arg);
    case SDD_UIF_IOCREINSERT:
        return uif_reinsert(ud);
    case SDD_UIF_IOCTBUSFREQ:
        return uif_set_bus_freq(ud, (int)arg);
    case SDD_UIF_IOCQMANFID:
        return uif_manf_id(ud);
    case SDD_UIF_IOCQCARDID:
        return uif_card_id(ud);
    case SDD_UIF_IOCQSTDIF:
        return uif_get_std_if(ud, arg);
    case SDD_UIF_IOCQMAXBLKSZ:
        return uif_get_max_block_size(ud, arg);
    case SDD_UIF_IOCQBLKSZ:
        return uif_get_block_size(ud, arg);
    case SDD_UIF_IOCTBLKSZ:
        return uif_set_block_size(ud, arg >> 8, arg & 0xff); /* low octet is function number */
    default:
        return -ENOTTY;
    }
}

static int uif_open(struct inode *inode, struct file *file)
{
    struct sdio_uif_dev *ud = container_of(inode->i_cdev, struct sdio_uif_dev, cdev);
    int ret = 0;

    mutex_lock(&ud->sdio_mutex);

    if (!ud->card_present)
        ret = -ENODEV;
    else {
        uif_get(ud);
        file->private_data = ud;
    }

    mutex_unlock(&ud->sdio_mutex);

    return ret;
}

static int uif_release(struct inode *inode, struct file *file)
{
    struct sdio_uif_dev *ud = container_of(inode->i_cdev, struct sdio_uif_dev, cdev);

    uif_put(ud);

    return 0;
}


static struct file_operations uif_fops = {
    .owner      = THIS_MODULE,
    .open       = uif_open,
    .release    = uif_release,
    .ioctl      = uif_ioctl,
};

static int uif_probe(struct sdio_dev *fdev)
{
    struct sdio_uif_dev *ud;
    dev_t devno;
    int ret = -ENOMEM;

    ud = kzalloc(sizeof(struct sdio_uif_dev), GFP_KERNEL);
    if (!ud)
        goto error_kzalloc;
    fdev->drv_data = ud;

    ud->fdev = fdev;

    cdev_init(&ud->cdev, &uif_fops);

    ud->card_present = 1;

    mutex_init(&ud->sdio_mutex);
    init_waitqueue_head(&ud->waitq);

    atomic_set(&ud->ref_count, 1);

    devno = MKDEV(MAJOR(uif_first_dev), MINOR(uif_first_dev) + ud->fdev->slot_id);
    ret = cdev_add(&ud->cdev, devno, 1);
    if (ret)
        goto error_cdev_add;

    if (!device_create(uif_class, NULL, ud->cdev.dev, NULL, "sdio_uif%d", ud->fdev->slot_id))
        goto error_device_create;

    return 0;

  error_device_create:
    cdev_del(&ud->cdev);
  error_cdev_add:
    kfree(ud);
  error_kzalloc:
    return ret;
}

static void uif_remove(struct sdio_dev *fdev)
{
    struct sdio_uif_dev *ud = fdev->drv_data;

    mutex_lock(&ud->sdio_mutex);
    ud->card_present = 0;
    mutex_unlock(&ud->sdio_mutex);

    device_destroy(uif_class, ud->cdev.dev);
    cdev_del(&ud->cdev);
    uif_put(ud);
}

static void uif_int_handler(struct sdio_dev *fdev)
{
    struct sdio_uif_dev *ud = fdev->drv_data;

    sdio_disable_interrupt(ud->fdev);
    ud->interrupt = 1;
    wake_up_interruptible(&ud->waitq);
}

static struct sdio_id_table uif_ids[] = {
    {
        .vendor_id = SDD_ANY_ID,
        .device_id = SDD_ANY_ID,
        .function  = SDD_UIF_FUNC,
        .interface = SDD_ANY_IFACE,
    },
    { 0 },
};

static struct sdio_func_driver uif_driver = {
    .name             = "uif",
    .id_table         = uif_ids,

    .probe            = uif_probe,
    .remove           = uif_remove,
    .card_int_handler = uif_int_handler,
};

static int __init sdio_uif_init(void)
{
    int ret;

    ret = alloc_chrdev_region(&uif_first_dev, 0, UIF_MAX_CARDS, "sdio_uif");
    if (ret)
        goto error;

    uif_class = class_create(THIS_MODULE, "sdio_uif");
    if (!uif_class)
        goto error;

    ret = sdio_driver_register(&uif_driver);
    if (ret < 0) {
        goto error;
    }

    return 0;

  error:
    if (uif_class)
        class_destroy(uif_class);
    if (uif_first_dev)
        unregister_chrdev_region(uif_first_dev, UIF_MAX_CARDS);
    return ret;
}

static void __exit sdio_uif_exit(void)
{
    sdio_driver_unregister(&uif_driver);

    class_destroy(uif_class);
    unregister_chrdev_region(uif_first_dev, UIF_MAX_CARDS);
}

module_init(sdio_uif_init);
module_exit(sdio_uif_exit);

MODULE_DESCRIPTION("SDIO Userspace Interface driver");
MODULE_AUTHOR("Cambridge Silicon Radio Ltd.");
MODULE_LICENSE("GPL and additional rights");
