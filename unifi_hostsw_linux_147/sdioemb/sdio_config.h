/*
 * Compile time configuration defaults.
 *
 * Copyright (C) 2007 Cambridge Silicon Radio Ltd.
 *
 * Refer to LICENSE.txt included with this source code for details on
 * the license terms.
 */
#ifndef _SDD_CONFIG_H
#define _SDD_CONFIG_H

/**
 * @defgroup config Compile-time configuration
 *
 * Various compile time options are set with \#define's.  These may be
 * overridden by providing appropriate compiler options (e.g., with
 * gcc's -D option).
 *
 * Defaults are provided in sdio_config.h.
 *
 *@{
 */

/**
 * @def SDD_SLOTS_MAX
 * 
 * Maximum number of SDIO slots that may be registered with the core.
 */ 
#ifndef SDD_SLOTS_MAX
#  define SDD_SLOTS_MAX 4
#endif

/**
 * @def SDD_FDRIVERS_MAX
 *
 * Maxium number of function drivers that may be registered with the
 * core.
 */
#ifndef SDD_FDRIVERS_MAX
#  define SDD_FDRIVERS_MAX 4
#endif

/**
 * @def SDD_FDEVS_MAX
 *
 * Maximum number of functions supported by the core.
 */
#ifndef SDD_FDEVS_MAX
#  define SDD_FDEVS_MAX 8
#endif

/**
 * @def SDD_CARD_INIT_RETRY_MAX
 *
 * Maximum number of times to try to initialize a card.
 */
#ifndef SDD_CARD_INIT_RETRY_MAX
#  define SDD_CARD_INIT_RETRY_MAX 3
#endif

/**
 * @def SDD_CARD_READY_TIMEOUT_MS
 *
 * Time (in ms) to wait for the card to become ready during the
 * initialization sequence.
 */
#ifndef SDD_CARD_READY_TIMEOUT_MS
#  define SDD_CARD_READY_TIMEOUT_MS 100
#endif

/**
 * @def SDD_CARD_IS_REMOVABLE
 *
 * Non-zero if the card is removable, zero if the card is always
 * inserted (e.g., it's wired directly to the SD host controller).
 *
 * A thread is used when detecting removable cards.
 */
#ifndef SDD_CARD_IS_REMOVABLE
#  define SDD_CARD_IS_REMOVABLE 1
#endif

/**
 * @def SDD_CSPI_REG_PADDING
 *
 * Amount of CSPI register padding in octets. The default of 0 should
 * be fine for all chips and bus speeds less than 25 MHz.
 */
#ifndef SDD_CSPI_REG_PADDING
#  define SDD_CSPI_REG_PADDING 0
#endif

/**
 * @def SDD_CSPI_BURST_PADDING
 *
 * Amount of CSPI burst padding in octets. The default of 2 should be
 * fine for all chips and bus speeds less than 25 MHz.
 */
#ifndef SDD_CSPI_BURST_PADDING
#  define SDD_CSPI_BURST_PADDING 2
#endif

/*@}*/

#endif /* _SDD_CONFIG_H */
