/*
 * Operating system kernel abstraction -- memory allocation
 *
 * Copyright (C) 2007 Cambridge Silicon Radio Ltd.
 *
 * Refer to LICENSE.txt included with this source code for details on
 * the license terms.
 */
#ifndef __OSKA_ALLOC_H
#define __OSKA_ALLOC_H

#ifdef __OSKA_API_DOC
/**
 * @defgroup alloc Memory allocation
 */

/**
 * Allocate a buffer of memory from the heap (or similar) and zero it.
 *
 * The allocated memory will be physically contiguous and therefore
 * suitable for DMA.
 *
 * This is intended for small size allocations less than a page;
 * larger, multi-page allocations are permitted but should be avoided.
 * See os_alloc_big() for an alternative for large allocations.
 *
 * The memory should be freed with os_free().
 *
 * Callable from: thread context.
 *
 * @param size memory size to allocate in octets.
 *
 * @return pointer to the allocated memory; or NULL if no memory could
 * be allocated.
 *
 * @ingroup alloc
 */
void *os_alloc(size_t size);

/**
 * Free memory allocated with os_alloc().
 *
 * Callable from: any context.
 *
 * @param ptr pointer to the buffer to free.
 *
 * @ingroup alloc
 */
void os_free(void *ptr);

/**
 * Allocate memory for large (multi-page) buffers.
 *
 * The allocated memory is \e not zeroed.  It may not be physically
 * contiguous and is \e not suitable for DMA.
 *
 * The memory should be freed with os_free_big().
 *
 * @ingroup alloc
 */
void *os_alloc_big(size_t size);

/**
 * Free memory allocated with os_alloc_big().
 *
 * Callable from: any context.
 *
 * @param ptr pointer to the buffer to free.
 *
 * @ingroup alloc
 */
void os_free_big(void *ptr);

#endif /* __OSKA_API_DOC */

#ifdef linux
#  include <../linux/alloc.h>
#else
#  error <oska/alloc.h> not provided for this OS
#endif

#endif /* #ifndef __OSKA_ALLOC_H */
