/*
 * ---------------------------------------------------------------------------
 *
 * FILE : unifi.h
 * 
 * PURPOSE : Public API for the driver core.
 * 
 * Copyright (C) 2005-2008 by Cambridge Silicon Radio Ltd.
 *
 * Refer to LICENSE.txt included with this source code for details on
 * the license terms.
 *
 * ---------------------------------------------------------------------------
 */
#ifndef __UNIFI_H__
#define __UNIFI_H__ 1


/* SDIO chip ID numbers */

/* Manufacturer id */
#define SDIO_MANF_ID_CSR              0x032A

/* Device id */
#define SDIO_CARD_ID_UNIFI_1          0x0001
#define SDIO_CARD_ID_UNIFI_2          0x0002
#define SDIO_CARD_ID_UNIFI_3          0x0007
#define SDIO_CARD_ID_UNIFI_4          0x0008

/* Function number for WLAN */
#define SDIO_WLAN_FUNC_ID_UNIFI_1          0x0001
#define SDIO_WLAN_FUNC_ID_UNIFI_2          0x0001
#define SDIO_WLAN_FUNC_ID_UNIFI_3          0x0001
#define SDIO_WLAN_FUNC_ID_UNIFI_4          0x0002

/* Maximum SDIO bus clock supported. */
#define UNIFI_SDIO_CLOCK_MAX    25000 /* kHz */

/* I/O default block size to use for UniFi. */
#define UNIFI_IO_BLOCK_SIZE     64

/*
 * The number of slots in the from-host queues.
 *
 * UNIFI_SOFT_TRAFFIC_Q_LENGTH is the number of slots in the traffic queues
 * and there will be UNIFI_WME_NO_OF_QS of them.
 * Traffic queues are used for data packets.
 * 
 * UNIFI_SOFT_COMMAND_Q_LENGTH is the number of slots in the command queue.
 * The command queue is used for MLME management requests.
 * 
 * Queues are ring buffers and so must always have 1 unused slot.
 */
#define UNIFI_SOFT_TRAFFIC_Q_LENGTH (20+1)
#define UNIFI_SOFT_COMMAND_Q_LENGTH (8+1)


/*
 * Structure describing a bulk data slot.
 * The length field is used to indicate empty/occupied state.
 * Needs to be defined before #include "unifi_os.h".
 */
typedef struct _bulk_data_desc
{
    const unsigned char *os_data_ptr;
    unsigned int data_length;
    const void *os_net_buf_ptr;
    unsigned int net_buf_length;
} bulk_data_desc_t;


/* unifi_os.h should be included only in unifi.h */
#include "unifi_os.h"           /* OS definitions from platform directory */

#include "unifi_config.h"       /* driver parameters from platform directory */
#include "driver/unifi_types.h" /* from this dir */
#include "driver/signals.h"     /* from this dir */
#include "hostio/hip_fsm_types.h"
#include "smeio/smeio_fsm_types.h"

/* 
 * unifi_os.h is responsible for providing definitions for:
 *   EIO
 *   EINVAL
 *   ENODEV
 *   ENOMEM
 * The values must be distinct, positive integer numbers.
 */


/* 
 * The card structure is an opaque pointer that is used to pass context
 * to the upper-edge API functions.
 */
typedef struct card card_t;


/*
 * This structure describes all of the bulk data that 'might' be
 * associated with a signal.
 */
typedef struct _bulk_data_param
{
    bulk_data_desc_t d[UNIFI_MAX_DATA_REFERENCES];
} bulk_data_param_t;


/*
 * This structure describes the chip and core information
 * that the core exposes to the OS layer.
 */
typedef struct _card_info
{
    uint16 chip_id;
    uint16 chip_version;
    uint32 fw_build;
    uint16 fw_hip_version;
    uint32 sdio_block_size;
} card_info_t;


/* UniFi Deep Sleep Signaling */
enum unifi_low_power_mode
{
    UNIFI_LOW_POWER_DISABLED,
    UNIFI_LOW_POWER_ENABLED
};

/* Periodic Wake Host Mode */
enum unifi_periodic_wake_mode
{
    UNIFI_PERIODIC_WAKE_HOST_DISABLED,
    UNIFI_PERIODIC_WAKE_HOST_ENABLED
};

/* 
 * Functions that read a portion of a firmware file.
 */
#define UNIFI_FW_LOADER 0
#define UNIFI_FW_STA    1
void* unifi_fw_read_start(void *ospriv, int is_fw);
void unifi_fw_read_stop(void *ospriv, void *dlpriv);
int unifi_fw_read(void *ospriv, void *arg, int offset, void *buf, int len);




/*
 *  UniFi Upper Edge interface
 */

/* Old SDIO init call - deprecated */
card_t *unifi_sdio_init(void *sdio, void *ospriv);

/* UniFi driver core initialisation */
card_t *unifi_alloc_card(void *iopriv, void *ospriv);
int unifi_init_card(card_t *card, int led_mask);
/*
 * unifi_init() and unifi_download() implement a subset of unifi_init_card functionality
 * that excludes HIP initialization. 
 * They should not be used at the same time with unifi_init_card.
 */
int unifi_init(card_t *card);

int unifi_download(card_t *card, int led_mask);

void unifi_free_card(card_t *card);


/* 
 * Send signal - Receive event.
 */
int unifi_send_signal(card_t *card, const unsigned char *sigptr, int siglen,
                      const bulk_data_param_t *bulkdata);
void unifi_receive_event(void *ospriv,
                         unsigned char *sigdata, int siglen,
                         const bulk_data_param_t *bulkdata);
int unifi_send_resources_available(card_t *card, const unsigned char *sigptr);

/* General management functions */
void unifi_card_info(card_t *card, card_info_t *card_info);

int unifi_check_io_status(card_t *card);


/* Botton-Half */
int unifi_bh(card_t *card, unsigned long *remaining);
int unifi_run_bh(void *ospriv);

/* UniFi Low Power Mode (Deep Sleep Signaling) */
int unifi_configure_low_power_mode(card_t *card,
                                   enum unifi_low_power_mode low_power_mode,
                                   enum unifi_periodic_wake_mode periodic_wake_mode);
int unifi_force_low_power_mode(card_t *card);

/* 
 * Call-outs from driver core to OS layer.
 */
/* Flow control callbacks */
void unifi_pause_xmit(void *ospriv);
void unifi_restart_xmit(void *ospriv);




/**
 * @defgroup bottomedge Bottom edge (SDIO driver interface) API
 */

int unifi_sdio_enable(void *sdio);
int unifi_sdio_disable(void *sdio);
void unifi_sdio_active(void *sdio);
void unifi_sdio_idle(void *sdio);
int unifi_sdio_readb(void *sdio, int funcnum,
                     unsigned long addr, unsigned char *pdata);
int unifi_sdio_writeb(void *sdio, int funcnum,
                      unsigned long addr, unsigned char data);
int unifi_sdio_block_rw(void *sdio, int funcnum,
                        unsigned long addr, unsigned char *pdata,
                        unsigned int count, int dir_is_write);
int unifi_sdio_enable_interrupt(void *sdio, int enable);
int unifi_sdio_set_max_clock_speed(void *sdio, int max_khz);

/**
 * 
 * Prevent the SDIO driver from removing power from the card.
 *
 * If the card is unpowered, switch on power to the card, and
 * initialize the SDIO interface (including re-writing CCCR register
 * for bus width, block size etc.).
 *
 * @param sdio the SDIO function context.
 *
 * @return \b 1 if the power is turned on and UniFi is initialised.
 *
 * @return \b 0 if the power is turned on and UniFi needs
 * to be initialized.
 *
 * @return \b -EIO if an I/O error occured while re-initializing the
 * card.  This is a fatal, non-recoverable error.
 *
 * @return \b -ENODEV if the card is no longer present.
 *
 * @ingroup bottomedge
 */
int unifi_sdio_power_on(void *sdio);

/**
 * Permit the SDIO driver to remove power.
 *
 * The SDIO driver may choose to retain power or switch off the power
 * some time after unifi_sdio_power_off() returns (perhaps because the
 * power rail supplies other devices or it is not controllable).
 *
 * After calling this function unifi_sdio_power_on() must be called
 * prior to performing further accesses to the card.
 *
 * @param sdio the SDIO function context.
 *
 * @ingroup bottomedge
 */
void unifi_sdio_power_off(void *sdio);

/**
 * Notify the SDIO driver that the platform is going into the
 * suspended state. This function may be called in place of 
 * unifi_sdio_power_off() if UniFi power is to be maintained.
 *
 * The SDIO driver should stop the SDIO clock and take any other
 * action to place the SDIO controller hardware into a low-power
 * state.
 *
 * @param sdio the SDIO function context.
 *
 * @ingroup bottomedge
 */
void unifi_sdio_suspend(void *sdio);

/**
 * Notify the SDIO driver that the platform is resuming from a
 * suspended state. This function may be called in place of 
 * unifi_sdio_power_on() if UniFi power was maintained during
 * suspend.
 *
 * The SDIO driver should take any action necessary to restore the
 * SDIO controller to a working state.
 *
 * @param sdio the SDIO function context.
 *
 * @ingroup bottomedge
 */
void unifi_sdio_resume(void *sdio);

/**
 * Attempt a hard reset of the UniFi chip.
 *
 * If the UniFi Chip is reset (by pulsing the chip's RESET# line low)
 * the SDIO driver must reinitialize the card (including re-writing
 * the CCCR register for bus width, block size etc.).
 *
 * @param sdio the SDIO function context.
 *
 * @return \b 1 if the SDIO driver is not capable of doing a hard
 * reset.
 *
 * @return \b 0 if a hard reset was successfully performed.
 *
 * @return \b -EIO if an I/O error occured while re-initializing the
 * card.  This is a fatal, non-recoverable error.
 *
 * @return \b -ENODEV if the card is no longer present.
 *
 * @ingroup bottomedge
 */
int unifi_sdio_hard_reset(void *sdio);


void unifi_sdio_interrupt_handler(card_t *card);


/* SDIO information */
enum unifi_sdio_request
{
    UNIFI_SDIO_IO_BLOCK_SIZE,
    UNIFI_SDIO_VENDOR_ID,
    UNIFI_SDIO_DEVICE_ID,
    UNIFI_SDIO_FUNCTION_NUM
};
int unifi_sdio_get_info(void *sdio, enum unifi_sdio_request param_type, unsigned int *param_value);


/* Functions to lookup Unifi signal and bulk data command names. */
const char *lookup_signal_name(int id);
const char *lookup_bulkcmd_name(int id);



/**
 * Configure the Traffic Analysis sampling
 *
 * Enable or disable statistics gathering.
 * Enable or disable particular packet detection.
 *
 * @param card the HIP core context
 * @param config_type the item to configure
 * @param config pointer to struct containing config info
 *
 * @return \b 0 if configuration was successful
 *
 * @return \b -EINVAL if a parameter had an invalid value
 *
 * @ingroup trafficanalysis
 */
int unifi_ta_configure(card_t *card,
                       unifi_traffic_configtype config_type,
                       const unifi_traffic_config *config);

void unifi_ta_sample(card_t *card,
                     unifi_protocol_direction direction,
                     const bulk_data_desc_t *data,
                     const unsigned char *saddr,
                     const unsigned char *sta_macaddr,
                     uint32 timestamp);

void unifi_ta_classification(card_t *card,
                             unifi_traffic_type traffic_type,
                             uint16 period);


/* 
 * Driver must provide these.
 *
 * A simple implementation will just call
 * unifi_sys_traffic_protocol_ind() or unifi_sys_traffic_classification_ind()
 * respectively. See sme_csr_userspace/sme_userspace.c.
 */
void unifi_ta_indicate_protocol(void *ospriv,
                                unifi_traffic_packettype packet_type,
                                unifi_protocol_direction direction,
                                const unifi_MACAddress *src_addr);

void unifi_ta_indicate_sampling(void *ospriv, unifi_traffic_stats *stats);

#endif /* __UNIFI_H__ */
