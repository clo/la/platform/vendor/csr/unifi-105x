/*
 * ---------------------------------------------------------------------------
 *
 * FILE: sdio_mmc_fs.c
 *
 *  This file provides the portable driver bottom-edge API implementation
 *  for the Freescale imx31ads platform, using the Freescale SDIO glue module.
 *
 * Copyright (C) 2007-2008 by Cambridge Silicon Radio Ltd.
 *
 * Refer to LICENSE.txt included with this source code for details on
 * the license terms.
 *
 * ---------------------------------------------------------------------------
 */

#include <linux/moduleparam.h>
#include <linux/module.h>
#include <linux/init.h>

#include "unifi_priv.h"

#include "sdio_freescale/fs_sdio_api.h"


static int fs_probe(struct sdio_dev *fdev);
static void fs_remove(struct sdio_dev *fdev);


int
unifi_sdio_readb(void *sdio, int funcnum, unsigned long addr,
                 unsigned char *pdata)
{

    return fs_sdio_readb(sdio, funcnum, addr, pdata);
}


int
unifi_sdio_writeb(void *sdio, int funcnum, unsigned long addr,
                  unsigned char data)
{
    return fs_sdio_writeb(sdio, funcnum, addr, data);
}


int
unifi_sdio_block_rw(void *sdio, int funcnum, unsigned long addr,
                    unsigned char *pdata, unsigned int count, int direction)
{
    return fs_sdio_block_rw(sdio, funcnum, addr,pdata, count, direction);
}



int
unifi_sdio_enable_interrupt(void *sdio, int enable)
{
    return fs_sdio_enable_interrupt(sdio, enable);
}



int
unifi_sdio_get_info(void *sdio, enum unifi_sdio_request param_type, unsigned int *param_value)
{
    struct sdio_dev *fdev = (struct sdio_dev*) sdio;

    switch (param_type) {
      case UNIFI_SDIO_IO_BLOCK_SIZE:

        *param_value = fdev->max_blocksize;
        break;

      case UNIFI_SDIO_VENDOR_ID:

        *param_value = fdev->vendor_id;
        break;

      case UNIFI_SDIO_DEVICE_ID:

        *param_value = fdev->device_id;
        break;

      default:
        return -EINVAL;
    }

    return 0;

} /* unifi_sdio_get_info() */

void
unifi_sdio_active(void *sdio)
{
}

void
unifi_sdio_idle(void *sdio)
{
}


int
unifi_sdio_enable(void *sdio)
{
    return fs_sdio_enable(sdio);
}


int
unifi_sdio_set_max_clock_speed(void *sdio, int max_khz)
{
    /* Respect the max set by the module parameter. */
    if (max_khz <= 0 || max_khz > sdio_clock) {
        max_khz = sdio_clock;
    }

    return fs_sdio_set_max_clock_speed(sdio, max_khz);

} /* unifi_sdio_set_max_clock_speed() */


int
unifi_sdio_hard_reset(void *sdio)
{
    return fs_sdio_hard_reset(sdio);
} /* unifi_sdio_hard_reset() */



static void
fs_int_handler(struct sdio_dev *fdev)
{
    unifi_priv_t *priv = fdev->drv_data;

    unifi_sdio_interrupt_handler(priv->card);
}



#ifdef CONFIG_PM
static void fs_suspend(struct sdio_dev *fdev, pm_message_t state)
{
    unifi_priv_t *priv = (unifi_priv_t*)fdev->drv_data;

    unifi_trace(priv, UDBG3, "Suspending...\n");
    /* Pass event to UniFi Driver. */
    unifi_suspend(priv);

}

static void fs_resume(struct sdio_dev *fdev)
{
    unifi_priv_t *priv = (unifi_priv_t*)fdev->drv_data;

    unifi_trace(priv, UDBG3, "Resuming...\n");
    /* Pass event to UniFi Driver. */
    unifi_resume(priv);

}
#else
#define mmc_uf_suspend  NULL
#define mmc_uf_resume   NULL
#endif


static struct fs_driver fs_driver = {
	.name	= "fs_driver",
	.probe		= fs_probe,
	.remove		= fs_remove,
	.card_int_handler = fs_int_handler,
	.suspend	= fs_suspend,
	.resume		= fs_resume,
};


int __init
uf_sdio_load(void)
{
    printk("Unifi: Using MMC/SDIO glue driver\n");
    return fs_sdio_register_driver(&fs_driver);
}


void __exit
uf_sdio_unload(void)
{
    fs_sdio_unregister_driver(&fs_driver);
}


static int fs_probe(struct sdio_dev *fdev)
{
    void *dev_priv;

    unifi_info(NULL, "card inserted\n");

    /* Always override default SDIO bus clock */
    unifi_trace(NULL, UDBG1, "Setting SDIO bus clock to %d kHz\n", sdio_clock);
    fs_sdio_set_max_clock_speed(fdev, sdio_clock);


    /* Register this device with the main UniFi driver */
    dev_priv = (void*)register_unifi_sdio(fdev, 0, NULL);

    fdev->drv_data = dev_priv;

    return 0;
}

static void fs_remove(struct sdio_dev *fdev)
{
    unifi_info(NULL, "card removed\n");

    /* Clean up the main UniFi driver */
    unregister_unifi_sdio(0);

    return;
}

