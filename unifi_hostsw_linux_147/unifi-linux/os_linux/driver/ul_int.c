/*
 * ***************************************************************************
 *  FILE:     ul_int.c
 *
 *  PURPOSE:
 *      Manage list of client applications using UniFi.
 *
 * Copyright (C) 2006-2008 by Cambridge Silicon Radio Ltd.
 *
 * Refer to LICENSE.txt included with this source code for details on
 * the license terms.
 *
 * ***************************************************************************
 */
#include "driver/unifi.h"
#include "driver/conversions.h"
#include "unifi_priv.h"
#include "unifiio.h"

/* The start of the range of process ids allocated for ul clients */
#define UDI_SENDER_ID_BASE      0xC000
#define UDI_SENDER_ID_SHIFT     8



/*
 * ---------------------------------------------------------------------------
 *  ul_init_clients
 *
 *      Initialise the clients array to empty.
 *
 *  Arguments:
 *      priv            Pointer to device private context struct
 *
 *  Returns:
 *      None.
 *
 *  Notes:
 *      This function needs to be called before priv is stored in
 *      Unifi_instances[].
 * ---------------------------------------------------------------------------
 */
void
ul_init_clients(unifi_priv_t *priv)
{
    int id;
    ul_client_t *ul_clients;

    init_MUTEX(&priv->udi_logging_mutex);
    priv->logging_client = NULL;

    ul_clients = priv->ul_clients;

    for (id = 0; id < MAX_UDI_CLIENTS; id++) {
        memset(&ul_clients[id], 0, sizeof(ul_client_t));

        ul_clients[id].client_id = id;
        ul_clients[id].sender_id = UDI_SENDER_ID_BASE + (id << UDI_SENDER_ID_SHIFT);
        ul_clients[id].instance = -1;
        ul_clients[id].event_hook = NULL;

        INIT_LIST_HEAD(&ul_clients[id].udi_log);
        init_waitqueue_head(&ul_clients[id].udi_wq);
        sema_init(&ul_clients[id].udi_sem, 1);

        ul_clients[id].wake_up_wq_id = 0;
        ul_clients[id].seq_no = 0;
        ul_clients[id].wake_seq_no = 0;
        ul_clients[id].snap_filter.count = 0;
    }
} /* ul_init_clients() */


/*
 * ---------------------------------------------------------------------------
 *  ul_register_client
 *
 *      This function registers a new ul client.
 *
 *  Arguments:
 *      priv            Pointer to device private context struct
 *      configuration   Special configuration for the client.
 *      udi_event_clbk  Callback for receiving event from unifi.
 *
 *  Returns:
 *      0 if a new clients is registered, -1 otherwise.
 * ---------------------------------------------------------------------------
 */
ul_client_t *
ul_register_client(unifi_priv_t *priv, unsigned int configuration,
                   udi_event_t udi_event_clbk)
{
    unsigned char id, ref;
    ul_client_t *ul_clients;

    ul_clients = priv->ul_clients;

    /* check for an unused entry */
    for (id = 0; id < MAX_UDI_CLIENTS; id++) {
        if (ul_clients[id].udi_enabled == 0) {
            ul_clients[id].instance = priv->instance;
            ul_clients[id].udi_enabled = 1;
            ul_clients[id].configuration = configuration;

            /* Allocate memory for the reply signal.. */
            ul_clients[id].reply_signal = (CSR_SIGNAL*) unifi_malloc(priv, sizeof(CSR_SIGNAL));
            if (ul_clients[id].reply_signal == NULL) {
                unifi_error(priv, "Failed to allocate reply signal for client.\n");
                return NULL;
            }
            /* .. and the bulk data of the reply signal. */
            for (ref = 0; ref < UNIFI_MAX_DATA_REFERENCES; ref ++) {
                ul_clients[id].reply_bulkdata[ref] =
                        (bulk_data_t*) unifi_malloc(priv, sizeof(bulk_data_t));
                /* If allocation fails, free allocated memory. */
                if (ul_clients[id].reply_bulkdata[ref] == NULL) {
                    for (; ref > 0; ref --) {
                        unifi_free(priv, ul_clients[id].reply_bulkdata[ref - 1]);
                    }
                    unifi_free(priv, ul_clients[id].reply_signal);
                    unifi_error(priv, "Failed to allocate bulk data buffers for client.\n");
                    return NULL;
                }
            }

            /* Set the event callback. */
            ul_clients[id].event_hook = udi_event_clbk;

            unifi_trace(priv, UDBG2, "UDI %d (0x%x) registered. configuration = 0x%x\n",
                        id, &ul_clients[id], configuration);
            return &ul_clients[id];
        }
    }
    return NULL;
} /* ul_register_client() */


/*
 * ---------------------------------------------------------------------------
 *  ul_deregister_client
 *
 *      This function deregisters a blocking UDI client.
 *
 *  Arguments:
 *      client      Pointer to the client we deregister.
 *
 *  Returns:
 *      0 if a new clients is deregistered.
 * ---------------------------------------------------------------------------
 */
int
ul_deregister_client(ul_client_t *ul_client)
{
    struct list_head *pos, *n;
    udi_log_t *logptr;
    unifi_priv_t *priv = unifi_find_instance(ul_client->instance);
    int ref;

    ul_client->instance = -1;
    ul_client->event_hook = NULL;
    ul_client->udi_enabled = 0;
    unifi_trace(priv, UDBG5, "UDI (0x%x) deregistered.\n", ul_client);

    /* Free memory allocated for the reply signal and its bulk data. */
    unifi_free(priv, ul_client->reply_signal);
    for (ref = 0; ref < UNIFI_MAX_DATA_REFERENCES; ref ++) {
        unifi_free(priv, ul_client->reply_bulkdata[ref]);
    }

    if (ul_client->snap_filter.count) {
        ul_client->snap_filter.count = 0;
        unifi_free(priv, ul_client->snap_filter.protocols);
    }

    /* Free anything pending on the udi_log list */
    down(&ul_client->udi_sem);
    list_for_each_safe(pos, n, &ul_client->udi_log)
    {
        logptr = list_entry(pos, udi_log_t, q);
        list_del(pos);
        kfree(logptr);
    }
    up(&ul_client->udi_sem);

    return 0;
} /* ul_deregister_client() */



/*
 * ---------------------------------------------------------------------------
 *  logging_handler
 *
 *      This function is registered with the driver core.
 *      It is called every time a UniFi HIP Signal is sent. It iterates over
 *      the list of processes interested in receiving log events and
 *      delivers the events to them.
 *
 *  Arguments:
 *      ospriv      Pointer to driver's private data.
 *      sigdata     Pointer to the packed signal buffer.
 *      signal_len  Length of the packed signal.
 *      bulkdata    Pointer to the signal's bulk data.
 *      dir         Direction of the signal
 *                  0 = from-host
 *                  1 = to-host
 *
 *  Returns:
 *      None.
 * ---------------------------------------------------------------------------
 */
void
logging_handler(void *ospriv, unsigned char *sigdata, int signal_len,
                const bulk_data_param_t *bulkdata, enum udi_log_direction direction)
{
    unifi_priv_t *priv = (unifi_priv_t*)ospriv;
    ul_client_t *client;
    int dir;

    dir = (direction == UDI_LOG_FROM_HOST) ? UDI_FROM_HOST : UDI_TO_HOST;

    down(&priv->udi_logging_mutex);
    client = priv->logging_client;
    if (client != NULL) {
        client->event_hook(client, sigdata, signal_len,
                           bulkdata, dir);
    }
    up(&priv->udi_logging_mutex);

} /* logging_handler() */




/*
 * ---------------------------------------------------------------------------
 *  send_client
 *
 *      Helper for unifi_receive_event.
 *
 *      This function forwards a signal to one client.
 *
 *  Arguments:
 *      priv        Pointer to driver's private data.
 *      client      Pointer to the client structure.
 *      receiver_id The reciever id of the signal.
 *      sigdata     Pointer to the packed signal buffer.
 *      siglen      Length of the packed signal.
 *      bulkdata    Pointer to the signal's bulk data.
 *
 *  Returns:
 *      None.
 *
 * ---------------------------------------------------------------------------
 */
static void send_to_client(unifi_priv_t *priv, ul_client_t *client,
                           int receiver_id,
                           unsigned char *sigdata, int siglen,
                           const bulk_data_param_t *bulkdata)
{
    if (client && client->event_hook) {
        unifi_trace(priv, UDBG3,
                    "Receive: client %d, (s:0x%X, r:0x%X) - Signal %s \n",
                    client->client_id, client->sender_id, receiver_id,
                    lookup_signal_name(UNPACK16((sigdata), 0)));

        client->event_hook(client, sigdata, siglen, bulkdata, UDI_TO_HOST);
    }
}


/*
 * ---------------------------------------------------------------------------
 *  unifi_receive_event
 *
 *      Dispatcher for received signals.
 *
 *      This function receives the 'to host' signals and forwards
 *      them to the unifi linux clients.
 *
 *  Arguments:
 *      ospriv      Pointer to driver's private data.
 *      sigdata     Pointer to the packed signal buffer.
 *      siglen      Length of the packed signal.
 *      bulkdata    Pointer to the signal's bulk data.
 *
 *  Returns:
 *      None.
 *
 *  Notes:
 *  The signals are received in the format described in the host interface
 *  specification, i.e wire formatted. Certain clients use the same format
 *  to interpret them and other clients use the host formatted structures.
 *  Each client has to call read_unpack_signal() to transform the wire
 *  formatted signal into the host formatted signal, if necessary.
 *  The code is in the core, since the signals are defined therefore
 *  binded to the host interface specification.
 * ---------------------------------------------------------------------------
 */
void
unifi_receive_event(void *ospriv,
                    unsigned char *sigdata, int siglen,
                    const bulk_data_param_t *bulkdata)
{
    unifi_priv_t *priv = (unifi_priv_t*)ospriv;
    int i, receiver_id;
    int client_id;
    int16 signal_id;

    func_enter();

    unifi_trace(priv, UDBG5, "unifi_receive_event: "
                "%04x %04x %04x %04x %04x %04x %04x %04x (%d)\n",
                UNPACK16((sigdata), sizeof(int16)*0) & 0xFFFF,
                UNPACK16((sigdata), sizeof(int16)*1) & 0xFFFF,
                UNPACK16((sigdata), sizeof(int16)*2) & 0xFFFF,
                UNPACK16((sigdata), sizeof(int16)*3) & 0xFFFF,
                UNPACK16((sigdata), sizeof(int16)*4) & 0xFFFF,
                UNPACK16((sigdata), sizeof(int16)*5) & 0xFFFF,
                UNPACK16((sigdata), sizeof(int16)*6) & 0xFFFF,
                UNPACK16((sigdata), sizeof(int16)*7) & 0xFFFF, siglen);

    receiver_id = UNPACK16((sigdata), sizeof(int16)) & 0xFFF0;
    client_id = (receiver_id & 0x0F00) >> UDI_SENDER_ID_SHIFT;
    signal_id = UNPACK16((sigdata), 0);

    /* Signals with ReceiverId==0 are also reported to SME / WEXT */
    if (receiver_id == 0) {

        /*
         * We must not pass MA.UNITDATA-INDICATIONs to the SME because
         * we can not filter them in the handler. The reason is that we
         * need to pass some AMP related data, but the filtering needs
         * be done in the 802.11->802.3 translation to avoid extra process
         * in the data path.
         */
        if (signal_id != CSR_MA_UNITDATA_INDICATION_ID) {
            send_to_client(priv, priv->sme_cli,
                           receiver_id,
                           sigdata, siglen, bulkdata);
        }

#ifdef CSR_NATIVE_LINUX
        send_to_client(priv, priv->wext_client,
                       receiver_id,
                       sigdata, siglen, bulkdata);
#endif
    }

    if ((client_id < MAX_UDI_CLIENTS) &&
        (&priv->ul_clients[client_id] != priv->logging_client)) {
        send_to_client(priv, &priv->ul_clients[client_id],
                       receiver_id,
                       sigdata, siglen, bulkdata);
    }

    /*
     * Free bulk data buffers here unless it is a CSR_MA_UNITDATA_INDICATION
     */
    switch (signal_id)
    {
    case CSR_MA_UNITDATA_INDICATION_ID:
#ifdef UNIFI_SNIFF_ARPHRD
    case CSR_MA_SNIFFDATA_INDICATION_ID:
#endif
        break;

    default:
        for (i = 0; i < UNIFI_MAX_DATA_REFERENCES; i++) {
            if (bulkdata->d[i].data_length != 0) {
                unifi_net_data_free(priv, (void *)&bulkdata->d[i]);
            }
        }
    }

    func_exit();
} /* unifi_receive_event() */



/*
 * ---------------------------------------------------------------------------
 *  ul_log_config_ind
 *
 *      This function uses the client's register callback
 *      to indicate configuration information e.g core errors.
 *
 *  Arguments:
 *      priv        Pointer to driver's private data.
 *      conf_param  Pointer to the configuration data.
 *      len         Length of the configuration data.
 *
 *  Returns:
 *      None.
 * ---------------------------------------------------------------------------
 */
void
ul_log_config_ind(unifi_priv_t *priv, u8 *conf_param, int len)
{
#ifdef CSR_SUPPORT_SME
    if (priv->smepriv == NULL)
    {
        return;
    }
    if ((CONFIG_IND_ERROR == (*conf_param)) && (priv->wifi_on_state == wifi_on_in_progress)) {
        unifi_notice(priv, "ul_log_config_ind: wifi on in progress, suppress error\n");
    } else {
        /* wifi_off_ind (error or exit) */
        unifi_sys_wifi_off_ind(priv->smepriv, (unifi_ControlIndication)(*conf_param));
    }

#else
    bulk_data_param_t bulkdata;

    /*
     * If someone killed unifi_managed before the driver was unloaded
     * the g_drvpriv pointer is going to be NULL. In this case it is
     * safe to assume that there is no client to get the indication.
     */
    if (!priv) {
        unifi_notice(NULL, "uf_sme_event_ind: NULL priv\n");
        return;
    }

    /* Create a null bulkdata structure. */
    bulkdata.d[0].data_length = 0;
    bulkdata.d[1].data_length = 0;

    sme_native_log_event(priv->sme_cli, conf_param, sizeof(uint8),
                         &bulkdata, UDI_CONFIG_IND);

#endif /* CSR_SUPPORT_SME */

} /* ul_log_config_ind */


/*
 * ---------------------------------------------------------------------------
 *  ul_send_signal_unpacked
 *
 *      This function sends a host formatted signal to unifi.
 *
 *  Arguments:
 *      priv        Pointer to driver's private data.
 *      sigptr      Pointer to the signal.
 *      bulkdata    Pointer to the signal's bulk data.
 *
 *  Returns:
 *      O on success, error code otherwise.
 *
 *  Notes:
 *  The signals have to be sent in the format described in the host interface
 *  specification, i.e wire formatted. Certain clients use the host formatted
 *  structures. The write_pack() transforms the host formatted signal
 *  into the wired formatted signal. The code is in the core, since the signals
 *  are defined therefore binded to the host interface specification.
 * ---------------------------------------------------------------------------
 */
int
ul_send_signal_unpacked(unifi_priv_t *priv, const CSR_SIGNAL *sigptr,
                        const bulk_data_param_t *bulkdata)
{
    unsigned char sigbuf[UNIFI_PACKED_SIGBUF_SIZE];
    int r, packed_siglen;
    unsigned long lock_flags;

    r = write_pack(sigptr, sigbuf, &packed_siglen);
    if (r) {
        unifi_error(priv, "Malformed HIP signal in ul_send_signal_unpacked()\n");
        return r;
    }

    spin_lock_irqsave(&priv->send_signal_lock, lock_flags);
    r = unifi_send_signal(priv->card, sigbuf, packed_siglen, bulkdata);
    spin_unlock_irqrestore(&priv->send_signal_lock, lock_flags);

    return r;
} /* ul_send_signal_unpacked() */


/*
 * ---------------------------------------------------------------------------
 *  reset_driver_status
 *
 *      This function is called from ul_send_signal_raw() when it detects
 *      that the SME has sent a MLME-RESET request.
 * 
 *  Arguments:
 *      priv        Pointer to device private struct
 *
 *  Returns:
 *      None.
 * ---------------------------------------------------------------------------
 */
static void
reset_driver_status(unifi_priv_t *priv)
{
    priv->sta_wmm_capabilities = 0;
#ifdef CSR_NATIVE_LINUX
    priv->mc_list_count_stored = 0;
    priv->wext_conf.flag_associated = 0;
    priv->wext_conf.block_controlled_port = unifi_8021x_PortOpen;
    priv->wext_conf.bss_wmm_capabilities = 0;
    priv->wext_conf.disable_join_on_ssid_set = 0;
#endif
} /* reset_driver_status() */


/*
 * ---------------------------------------------------------------------------
 *  ul_send_signal_raw
 *
 *      This function sends a wire formatted data signal to unifi.
 *
 *  Arguments:
 *      priv        Pointer to driver's private data.
 *      sigptr      Pointer to the signal.
 *      siglen      Length of the signal.
 *      bulkdata    Pointer to the signal's bulk data.
 *
 *  Returns:
 *      O on success, error code otherwise.
 * ---------------------------------------------------------------------------
 */
int
ul_send_signal_raw(unifi_priv_t *priv, const unsigned char *sigptr, int siglen,
                   const bulk_data_param_t *bulkdata)
{
    int r;
    unsigned long lock_flags;

    spin_lock_irqsave(&priv->send_signal_lock, lock_flags);
    r = unifi_send_signal(priv->card, sigptr, siglen, bulkdata);
    spin_unlock_irqrestore(&priv->send_signal_lock, lock_flags);

    if (r) {
        return r;
    }

    /*
     * Since this is use by unicli, if we get an MLME reset request
     * we need to initialize a few status parameters
     * that the driver uses to make decisions.
     */
    if (GET_SIGNAL_ID(sigptr) == CSR_MLME_RESET_REQUEST_ID) {
        /* TODO: do not use this */
        reset_driver_status(priv);
    }

    return 0;
} /* ul_send_signal_raw() */


