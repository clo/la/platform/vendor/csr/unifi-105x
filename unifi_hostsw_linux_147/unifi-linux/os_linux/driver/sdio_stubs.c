/*
 * Stubs for some of the bottom edge functions.
 *
 * These stubs are optional functions in the bottom edge (SDIO driver
 * interface) API that not all platforms or SDIO drivers may support.
 *
 * They're declared as weak symbols so they can be overridden by
 * simply providing a non-weak declaration.
 *
 * Copyright (C) 2007-2008 by Cambridge Silicon Radio Ltd.
 *
 * Refer to LICENSE.txt included with this source code for details on
 * the license terms.
 *
 */
#include "driver/unifi.h"

void __attribute__((weak)) unifi_sdio_idle(void *sdio)
{
}

void __attribute__((weak)) unifi_sdio_active(void *sdio)
{
}

int __attribute__((weak)) unifi_sdio_power_on(void *sdio)
{
    return 0;
}

void __attribute__((weak)) unifi_sdio_power_off(void *sdio)
{
}

int __attribute__((weak)) unifi_sdio_hard_reset(void *sdio)
{
    return 1;
}

void __attribute__((weak)) unifi_sdio_suspend(void *sdio)
{
}

void __attribute__((weak)) unifi_sdio_resume(void *sdio)
{
}

void __attribute__((weak)) uf_sdio_claim(void *sdio)
{
}

void __attribute__((weak)) uf_sdio_release(void *sdio)
{
}
