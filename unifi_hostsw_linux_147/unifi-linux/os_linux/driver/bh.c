/*
 * ---------------------------------------------------------------------------
 * FILE:     bh.c
 *
 * PURPOSE:
 *      Driver bottom-half.
 *      This provides a bottom-half thread for the driver core.
 *
 * Copyright (C) 2005-2008 by Cambridge Silicon Radio Ltd.
 *
 * Refer to LICENSE.txt included with this source code for details on
 * the license terms.
 *
 * ---------------------------------------------------------------------------
 */
#include "driver/unifi.h"
#include "unifi_priv.h"


#define MAX_INIT_ATTEMPTS        4

static void handle_bh_error(unifi_priv_t *priv);

extern int led_mask;




/*
 * ---------------------------------------------------------------------------
 *  uf_start_thread
 *
 *      Start a new thread.
 *
 *  Arguments:
 *      priv            Pointer to OS driver structure for the device.
 *      thread          Pointer to the thread object
 *      func            The thread function
 *
 *  Returns:
 *      0 on success or else a Linux error code.
 * ---------------------------------------------------------------------------
 */
int
uf_start_thread(unifi_priv_t *priv, struct uf_thread *thread, int (*func)(void *))
{
    if (thread->thread_task != NULL) {
        unifi_error(priv, "%s thread already started\n", thread->name);
        return 0;
    }

    /* Start the kernel thread that handles all h/w accesses. */
    thread->thread_task = kthread_run(func, priv, thread->name);
    if (IS_ERR(thread->thread_task)) {
        return PTR_ERR(thread->thread_task);
    }

    unifi_trace(priv, UDBG2, "Started %s thread\n", thread->name);

    return 0;
} /* uf_start_thread() */


/*
 * ---------------------------------------------------------------------------
 *  uf_stop_thread
 *
 *      Stops a thread.
 *
 *  Arguments:
 *      priv            Pointer to OS driver structure for the device.
 *      thread          Pointer to the thread object
 *
 *  Returns:
 *
 * ---------------------------------------------------------------------------
 */
void
uf_stop_thread(unifi_priv_t *priv, struct uf_thread *thread)
{
    if (!thread->thread_task) {
        unifi_notice(priv, "%s thread is already stopped\n", thread->name);
        return;
    }

    unifi_trace(priv, UDBG2, "Stopping %s thread\n", thread->name);

    kthread_stop(thread->thread_task);
    thread->thread_task = NULL;

} /* uf_stop_thread() */



/*
 * ---------------------------------------------------------------------------
 *  uf_wait_for_thread_to_stop
 *
 *      Waits until a thread is stopped.
 *
 *  Arguments:
 *      priv            Pointer to OS driver structure for the device.
 *
 *  Returns:
 *
 * ---------------------------------------------------------------------------
 */
void
uf_wait_for_thread_to_stop(unifi_priv_t *priv, struct uf_thread *thread)
{
    /*
     * kthread_stop() cannot handle the thread exiting while
     * kthread_should_stop() is false, so sleep until kthread_stop()
     * wakes us up.
     */
    unifi_trace(priv, UDBG2, "%s waiting for the stop signal.\n", thread->name);
    set_current_state(TASK_INTERRUPTIBLE);
    if (!kthread_should_stop()) {
        unifi_trace(priv, UDBG2, "%s schedule....\n", thread->name);
        schedule();
    }

    thread->thread_task = NULL;
    unifi_trace(priv, UDBG2, "%s exiting....\n", thread->name);
} /* uf_wait_for_thread_to_stop() */


/*
 * ---------------------------------------------------------------------------
 *  bh_thread_function
 *
 *      All hardware access happens in this thread.
 *      This means there is no need for locks on the hardware and we don't need
 *      to worry about reentrancy with the SDIO library.
 *
 *  Arguments:
 *      arg             Pointer to OS driver structure for the device.
 *
 *  Returns:
 *      None.
 *
 *  Notes:
 *      When the bottom half of the driver needs to process signals, events,
 *      or simply the host status (i.e sleep mode), it invokes unifi_run_bh().
 *      Since we need all SDIO transaction to be in a single thread, the
 *      unifi_run_bh() will store the reason any wake up this thread to process it.
 *
 * ---------------------------------------------------------------------------
 */
static int
bh_thread_function(void *arg)
{
    unifi_priv_t *priv = (unifi_priv_t *)arg;
    int r;
    long ret;
    long timeout, t;
    struct uf_thread *this_thread;

    unifi_trace(priv, UDBG2, "bh_thread_function starting\n");

    this_thread = &priv->bh_thread;

    t = timeout = 0;
    while (!kthread_should_stop()) {
        /* wait until an error occurs, or we need to process something. */
        unifi_trace(priv, UDBG3, "bh_thread goes to sleep.\n");

        if (timeout > 0) {
            /* Convert t in ms to jiffies */
            t = msecs_to_jiffies(timeout);
            ret = wait_event_interruptible_timeout(this_thread->wakeup_q,
                                                   (this_thread->wakeup_flag && !this_thread->block_thread) ||
                                                   kthread_should_stop(),
                                                   t);
            timeout = (ret > 0) ? jiffies_to_msecs(ret) : 0;
        } else {
            ret = wait_event_interruptible(this_thread->wakeup_q,
                                           (this_thread->wakeup_flag && !this_thread->block_thread) ||
                                           kthread_should_stop());
        }

        if (kthread_should_stop()) {
            unifi_trace(priv, UDBG2, "bh_thread: signalled to exit\n");
            break;
        }

        if (ret < 0) {
            unifi_notice(priv,
                        "bh_thread: wait_event returned %d, thread will exit\n",
                        ret);
            uf_wait_for_thread_to_stop(priv, this_thread);
            break;
        }

        this_thread->wakeup_flag = 0;

        unifi_trace(priv, UDBG3, "bh_thread calls unifi_bh().\n");

        uf_sdio_claim(priv->sdio);
        r = unifi_bh(priv->card, &timeout);
        uf_sdio_release(priv->sdio);
        if (r == -ENODEV) {
            uf_wait_for_thread_to_stop(priv, this_thread);
            break;
        }
        if (r < 0) {
            /* Errors must be delivered to the error task */
            handle_bh_error(priv);
        }
    }

    /*
     * Interrupts might be enabled here, (unifi_bh enables them)
     * so we need to disable interrupts before return.
     */
    uf_sdio_claim(priv->sdio);
    unifi_sdio_enable_interrupt(priv->sdio, 0);
    uf_sdio_release(priv->sdio);

    unifi_trace(priv, UDBG2, "bh_thread exiting....\n");
    return 0;
} /* bh_thread_function() */




/*
 * ---------------------------------------------------------------------------
 *  uf_init_bh
 *
 *      Starts the bottom half of the driver.
 *      All we need to do here is start the I/O bh thread.
 *
 *  Arguments:
 *      priv            Pointer to OS driver structure for the device.
 *
 *  Returns:
 *      0 on success or else a Linux error code.
 *
 *  Notes:
 *      This is the first function in the sequence that the SME
 *      (or helper application) need to call in order to initialize unifi.
 * ---------------------------------------------------------------------------
 */
int
uf_init_bh(unifi_priv_t *priv)
{
    /* Enable mlme interface. */
    priv->io_aborted = 0;


    return uf_start_thread(priv, &priv->bh_thread, bh_thread_function);
} /* uf_init_bh() */


/*
 * ---------------------------------------------------------------------------
 *  handle_bh_error
 *
 *      This function reports an error to the SME (or the helper app).
 *      Normally, the SME will try to reset the device and go through
 *      the initialisation of the UniFi.
 *
 *  Arguments:
 *      priv            Pointer to OS driver structure for the device.
 *
 *  Returns:
 *      None.
 *
 *  Notes:
 * ---------------------------------------------------------------------------
 */
static void
handle_bh_error(unifi_priv_t *priv)
{
    u8 conf_param = CONFIG_IND_ERROR;


    /* Block unifi_run_bh() until the error has been handled. */
    priv->bh_thread.block_thread = 1;
    /* Consider UniFi to be uninitialised */
    priv->init_progress = UNIFI_INIT_NONE;

    /* Stop the network traffic */
    if (priv->netdev_registered == 1) {
        netif_carrier_off(priv->netdev);
    }

#ifdef CSR_NATIVE_LINUX
    /* Force any client waiting on an mlme_wait_for_reply() to abort. */
    unifi_abort_mlme(priv);
#endif /* CSR_NATIVE_LINUX */

    unifi_error(priv, "handle_bh_error: fatal error is reported to the SME.\n");
    /* Notify the clients (SME or unifi_manager) for the error. */
    ul_log_config_ind(priv, &conf_param, sizeof(u8));

} /* handle_bh_error() */



/*
 * ---------------------------------------------------------------------------
 *  unifi_run_bh
 *
 *      The bottom half of the driver calls this function when
 *      it wants to process anything that requires access to unifi.
 *      We need to call unifi_bh() which in this implementation is done
 *      by waking up the manager thread.
 *
 *  Arguments:
 *      ospriv          Pointer to OS driver structure for the device.
 *
 *  Returns:
 *      0 on success or else a Linux error code.
 *
 *  Notes:
 * ---------------------------------------------------------------------------
 */
int unifi_run_bh(void *ospriv)
{
    unifi_priv_t *priv = ospriv;

    /*
    * If an error has occured, we discard silently all messages from the bh
    * until the error has been processed and the unifi has been reinitialised.
    */
    if (priv->bh_thread.block_thread == 1) {
        unifi_trace(priv, UDBG3, "unifi_run_bh: discard message.\n");
        return -EIO;
    }

    priv->bh_thread.wakeup_flag = 1;
    /* wake up I/O thread */
    wake_up_interruptible(&priv->bh_thread.wakeup_q);

    return 0;
} /* unifi_run_bh() */



/*
 * ---------------------------------------------------------------------------
 *  uf_init_hw
 *
 *      Resets hardware, downloads and initialised f/w.
 *      This function will retry to initialise UniFi if unifi_init_card()
 *      returns an error.
 *
 *  Arguments:
 *      ospriv          Pointer to OS driver structure for the device.
 *
 *  Returns:
 *      O on success, non-zero otherwise.
 *
 * ---------------------------------------------------------------------------
 */
int
uf_init_hw(unifi_priv_t *priv)
{
    int attempts = 0;
    int r = -1;
    int priv_instance;

    priv_instance = unifi_find_priv(priv);
    if (priv_instance == -1) {
        unifi_warning(priv, "uf_init_hw: Unknown priv instance, will use fw_init[0]\n");
        priv_instance = 0;
    }

    while (1) {
        if (attempts > MAX_INIT_ATTEMPTS) {
            unifi_error(priv, "Failed to initialise UniFi after %d attempts, "
                        "giving up.\n",
                        attempts);
            break;
        }
        attempts++;

        unifi_info(priv, "Initialising UniFi, attempt %d\n", attempts);

        if (fw_init[priv_instance] > 0) {
            unifi_notice(priv, "f/w init prevented by module parameter\n");
            break;
        } else if (fw_init[priv_instance] == 0) {
            fw_init[priv_instance] ++;
        }

        /*
         * Initialise driver core. This will perform a reset of UniFi
         * internals, but not the SDIO CCCR.
         */
        uf_sdio_claim(priv->sdio);
        r = unifi_init_card(priv->card, led_mask);
        uf_sdio_release(priv->sdio);
        if (r == -ENODEV) {
            break;
        }
        if (r == -ENODATA) {
            unifi_error(priv, "Firmware file required, but not found.\n");
            break;
        }
        if (r < 0) {
            /* failed. Reset h/w and try again */
            unifi_error(priv, "Failed to initialise UniFi chip.\n");
            continue;
        }

        /* Enabled deep sleep signaling */
        unifi_configure_low_power_mode(priv->card,
                                       UNIFI_LOW_POWER_ENABLED,
                                       UNIFI_PERIODIC_WAKE_HOST_DISABLED);

        /* Enable bh processing */
        priv->bh_thread.block_thread = 0;

        /* Get the version information from the core */
        unifi_card_info(priv->card, &priv->card_info);

        return r;
    }

    return r;

} /* uf_init_hw */


/*
 * ---------------------------------------------------------------------------
 *
 *      F/W download. Part of the HIP driver API
 *
 * ---------------------------------------------------------------------------
 */


/*
 * ---------------------------------------------------------------------------
 *  unifi_fw_read_start
 *
 *      Returns a structure to be passed in unifi_fw_read().
 *      This structure is an OS specific description of the f/w file.
 *      In the linux implementation it is a buffer with the f/w and its' length.
 *      The HIP driver calls this functions to request for the loader or
 *      the firmware file.
 *      The structure pointer can be freed when unifi_fw_read_stop() is called.
 *
 *  Arguments:
 *      ospriv          Pointer to driver context.
 *      is_fw           Flag to indicate whether it is the f/w or the loader
 *
 *  Returns:
 *      O on success, non-zero otherwise.
 *
 * ---------------------------------------------------------------------------
 */
void*
unifi_fw_read_start(void *ospriv, int is_fw)
{
    unifi_priv_t *priv = (unifi_priv_t*)ospriv;

    func_enter();

    if (is_fw == UNIFI_FW_LOADER) {
        /* Set up callback struct for readfunc() */
        if (priv->fw_loader.dl_data != NULL) {
            func_exit();
            return &priv->fw_loader;
        }

    } else if (is_fw == UNIFI_FW_STA) {
        /* Set up callback struct for readfunc() */
        if (priv->fw_sta.dl_data != NULL) {
            func_exit();
            return &priv->fw_sta;
        }

    } else {
        unifi_error(priv, "downloading firmware... unknown request: %d\n", is_fw);
    }

    func_exit();
    return NULL;
} /* unifi_fw_read_start() */



/*
 * ---------------------------------------------------------------------------
 *  unifi_fw_read_stop
 *
 *      Called when the HIP driver has finished using the loader or
 *      the firmware file.
 *      The dlpriv pointer can be freed now.
 *
 *  Arguments:
 *      ospriv          Pointer to driver context.
 *      dlpriv          The pointer returned by unifi_fw_read_start()
 *
 * ---------------------------------------------------------------------------
 */
void
unifi_fw_read_stop(void *ospriv, void *dlpriv)
{
    /* EMPTY */
} /* unifi_fw_read_stop() */



/*
 * ---------------------------------------------------------------------------
 *  unifi_fw_read
 *
 *      The HIP driver calls this function to ask for a part of the loader or
 *      the firmware file.
 *
 *  Arguments:
 *      ospriv          Pointer to driver context.
 *      arg             The pointer returned by unifi_fw_read_start().
 *      offset          The offset in the file to return from.
 *      buf             A buffer to store the requested data.
 *      len             The size of the buf and the size of the requested data.
 *
 * ---------------------------------------------------------------------------
 */
int
unifi_fw_read(void *ospriv, void *arg, int offset, void *buf, int len)
{
    const struct dlpriv *dlpriv = arg;

    if (offset >= dlpriv->dl_len) {
        /* at end of file */
        return 0;
    }

    if ((offset + len) > dlpriv->dl_len) {
        /* attempt to read past end of file */
        return -1;
    }

    memcpy(buf, dlpriv->dl_data+offset, len);

    return len;

} /* unifi_fw_read() */



/*
 * ---------------------------------------------------------------------------
 *
 *      Suspend / Resume. Part of the HIP driver API
 *
 * ---------------------------------------------------------------------------
 */

/*
 * ---------------------------------------------------------------------------
 *  unifi_suspend
 *
 *      Handles a suspend request from the SDIO driver.
 *
 *  Arguments:
 *      ospriv          Pointer to OS driver context.
 *
 * ---------------------------------------------------------------------------
 */
void unifi_suspend(void *ospriv)
{
    unifi_priv_t *priv = ospriv;

    if (priv->netdev_registered == 1) {
        /* Stop network traffic. */
        netif_carrier_off(priv->netdev);
        UF_NETIF_TX_STOP_ALL_QUEUES(priv->netdev);
    }

    sme_sys_suspend(priv);
} /* unifi_suspend() */


/*
 * ---------------------------------------------------------------------------
 *  unifi_resume
 *
 *      Handles a resume request from the SDIO driver.
 *
 *  Arguments:
 *      ospriv          Pointer to OS driver context.
 *
 * ---------------------------------------------------------------------------
 */
void unifi_resume(void *ospriv)
{
    unifi_priv_t *priv = ospriv;
    int r;

    r = sme_sys_resume(priv);
    if (r) {
        unifi_error(priv, "Failed to resume UniFi\n");
    }

} /* unifi_resume() */


