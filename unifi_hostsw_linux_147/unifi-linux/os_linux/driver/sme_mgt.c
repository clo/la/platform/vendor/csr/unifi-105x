/*
 * ---------------------------------------------------------------------------
 * FILE:     sme_mgt.c
 *
 * PURPOSE:
 *      This file contains the driver specific implementation of
 *      the SME MGT SAP and implements the WEXT <==> SME MGT interface
 *      for all SME builds that support WEXT.
 *
 * Copyright (C) 2008 by Cambridge Silicon Radio Ltd.
 *
 * Refer to LICENSE.txt included with this source code for details on
 * the license terms.
 *
 * ---------------------------------------------------------------------------
 */

#include "driver/unifiversion.h"
#include "unifi_priv.h"
#include "driver/conversions.h"

/*
 * This file implements the SME MGT API. It contains the following functions:
 * unifi_mgt_wifi_flightmode_cfm()
 * unifi_mgt_wifi_on_cfm()
 * unifi_mgt_wifi_off_cfm()
 * unifi_mgt_wifi_off_ind()
 * unifi_mgt_scan_full_cfm()
 * unifi_mgt_scan_results_get_cfm()
 * unifi_mgt_scan_result_ind()
 * unifi_mgt_connect_cfm()
 * unifi_mgt_media_status_ind()
 * unifi_mgt_disconnect_cfm()
 * unifi_mgt_key_cfm()
 * unifi_mgt_multicast_address_cfm()
 * unifi_mgt_set_value_cfm()
 * unifi_mgt_get_value_cfm()
 * unifi_mgt_mic_failure_ind()
 * unifi_mgt_pmkid_cfm()
 * unifi_mgt_pmkid_candidate_list_ind()
 * unifi_mgt_mib_set_cfm()
 * unifi_mgt_mib_get_cfm()
 * unifi_mgt_mib_get_next_cfm()
 * unifi_mgt_connection_quality_ind()
 * unifi_mgt_packet_filter_set_cfm()
 * unifi_mgt_tspec_cfm()
 * unifi_mgt_tspec_ind()
 */


/*
 * Handling the wext requests, we need to block
 * until the SME sends the response to our request.
 * We use the sme_init_request() and sme_wait_for_reply()
 * to implement this behavior in the following functions:
 * sme_mgt_wifi_on()
 * sme_mgt_wifi_off()
 * sme_mgt_scan_full()
 * sme_mgt_scan_results_get_async()
 * sme_mgt_connect()
 * sme_mgt_disconnect()
 * sme_mgt_pmkid()
 * sme_mgt_key()
 * sme_mgt_get_versions()
 * sme_mgt_set_value_async()
 * sme_mgt_get_value_async()
 */



void unifi_mgt_mic_failure_ind(void *drvpriv, Boolean secondFailure,
                               uint16 count, const unifi_MACAddress* address,
                               unifi_KeyType keyType, uint16 keyId,
                               const unifi_SequenceCount* tSC)
{
    unifi_priv_t *priv = (unifi_priv_t*)drvpriv;
    CSR_MLME_MICHAELMICFAILURE_INDICATION mic_ind;

    if (priv == NULL) {
        unifi_error(NULL, "unifi_mgt_mic_failure_ind: invalid priv\n");
        return;
    }

    unifi_trace(priv, UDBG1,
                "unifi_mgt_mic_failure_ind: count=%d, KeyType=%d, KeyId=%d\n",
                count, keyType, keyId);

    mic_ind.Count = count;
    memcpy(mic_ind.Address.x, address->data, 6);
    mic_ind.KeyType = keyType;
    mic_ind.KeyId = keyId;
    mic_ind.Tsc = UNPACK64(tSC->data, 0);

    wext_send_michaelmicfailure_event(priv, &mic_ind);
}


void unifi_mgt_pmkid_cfm(void *drvpriv, unifi_Status status,
                         unifi_ListAction action,
                         const unifi_PmkidList* pmkids)
{
    unifi_priv_t *priv = (unifi_priv_t*)drvpriv;

    if (priv == NULL) {
        unifi_error(NULL, "unifi_mgt_pmkid_cfm: Invalid ospriv.\n");
        return;
    }

    /*
     * WEXT never does a GET operation the PMKIDs, so we don't need
     * handle data returned in pmkids.
     */

    sme_complete_request(priv, status);
}


void unifi_mgt_pmkid_candidate_list_ind(void *drvpriv,
                                        const unifi_PmkidCandidateList* candidates)
{
}


void unifi_mgt_scan_results_get_cfm(void *drvpriv, unifi_Status status,
                                    const unifi_ScanResultList* resultsBuffer)
{
    unifi_priv_t *priv = (unifi_priv_t*)drvpriv;
    int bytesRequired = resultsBuffer->numElements * sizeof(unifi_ScanResult);
    int i;
    uint8* current_buff;
    unifi_ScanResultList scanCopy;

    if (priv == NULL) {
        unifi_error(NULL, "unifi_mgt_scan_results_get_cfm: Invalid ospriv.\n");
        return;
    }

    /* Calc the size of the buffer reuired */
    for (i = 0; i < resultsBuffer->numElements; ++i) {
        unifi_ScanResult *scan_result = &resultsBuffer->results[i];
        bytesRequired += scan_result->informationElements.length;
    }

    /* Take a Copy of the scan Results :-) */
    scanCopy.numElements = resultsBuffer->numElements;
    scanCopy.results = unifi_malloc(priv, bytesRequired);
    memcpy(scanCopy.results, resultsBuffer->results, sizeof(unifi_ScanResult)* scanCopy.numElements);

    /* Take a Copy of the Info Elements AND update the scan result pointers */
    current_buff = (uint8*)&scanCopy.results[scanCopy.numElements];
    for (i = 0; i < scanCopy.numElements; ++i)
    {
        unifi_ScanResult *scan_result = &scanCopy.results[i];
        osa_memcpy(current_buff, scan_result->informationElements.data, scan_result->informationElements.length);
        scan_result->informationElements.data = current_buff;
        current_buff += scan_result->informationElements.length;
    }

    priv->sme_reply.reply_scan_results = scanCopy;

    sme_complete_request(priv, status);
}


void unifi_mgt_scan_full_cfm(void *drvpriv, unifi_Status status)
{
    unifi_priv_t *priv = (unifi_priv_t*)drvpriv;

    if (priv == NULL) {
        unifi_error(NULL, "unifi_mgt_scan_full_cfm: Invalid ospriv.\n");
        return;
    }

    sme_complete_request(priv, status);
}


void unifi_mgt_scan_result_ind(void *drvpriv, const unifi_ScanResult* result)
{

}


void unifi_mgt_connect_cfm(void *drvpriv, unifi_Status status)
{
    unifi_priv_t *priv = (unifi_priv_t*)drvpriv;

    if (priv == NULL) {
        unifi_error(NULL, "unifi_mgt_network_join_cfm: Invalid ospriv.\n");
        return;
    }

    sme_complete_request(priv, status);
}


void unifi_mgt_disconnect_cfm(void *drvpriv, unifi_Status status)
{
    unifi_priv_t *priv = (unifi_priv_t*)drvpriv;

    if (priv == NULL) {
        unifi_error(NULL, "unifi_mgt_disconnect_cfm: Invalid ospriv.\n");
        return;
    }

    sme_complete_request(priv, status);
}


void unifi_mgt_key_cfm(void *drvpriv, unifi_Status status,
                       unifi_ListAction action)
{
    unifi_priv_t *priv = (unifi_priv_t*)drvpriv;

    if (priv == NULL) {
        unifi_error(NULL, "unifi_mgt_key_cfm: Invalid ospriv.\n");
        return;
    }

    sme_complete_request(priv, status);
}


void unifi_mgt_multicast_address_cfm(void *drvpriv, unifi_Status status,
                                     unifi_ListAction action,
                                     const unifi_MulticastAddressList* addresses)
{
    unifi_priv_t *priv = (unifi_priv_t*)drvpriv;

    if (priv == NULL) {
        unifi_error(NULL, "unifi_mgt_multicast_address_cfm: Invalid ospriv.\n");
        return;
    }

    sme_complete_request(priv, status);
}

void unifi_mgt_wifi_flightmode_cfm(void *drvpriv, unifi_Status status)
{
    unifi_priv_t *priv = (unifi_priv_t*)drvpriv;

    if (priv == NULL) {
        unifi_error(NULL, "unifi_mgt_wifi_flightmode_cfm: Invalid ospriv.\n");
        return;
    }

    sme_complete_request(priv, status);
}

void unifi_mgt_wifi_on_cfm(void *drvpriv, unifi_Status status)
{
    unifi_priv_t *priv = (unifi_priv_t*)drvpriv;

    if (priv == NULL) {
        unifi_error(NULL, "unifi_mgt_wifi_on_cfm: Invalid ospriv.\n");
        return;
    }

    unifi_trace(priv, UDBG4,
                "unifi_mgt_wifi_on_cfm: wake up status %d\n", status);

#if 0
    sme_complete_request(priv, status);
#endif

}

void unifi_mgt_wifi_off_cfm(void *drvpriv, unifi_Status status)
{
    unifi_priv_t *priv = (unifi_priv_t*)drvpriv;

    if (priv == NULL) {
        unifi_error(NULL, "unifi_mgt_wifi_off_cfm: Invalid ospriv.\n");
        return;
    }

    sme_complete_request(priv, status);
}


void unifi_mgt_wifi_off_ind(void *drvpriv, unifi_ControlIndication status)
{
    unifi_priv_t *priv = (unifi_priv_t*)drvpriv;

    if (priv == NULL) {
        unifi_error(NULL, "unifi_sys_stopped_req: Invalid ospriv.\n");
        return;
    }

    if (priv->smepriv == NULL) {
        unifi_error(priv, "unifi_sys_stopped_req: invalid smepriv\n");
        return;
    }

    /*
     * If the status indicates an error, the SME is in a stopped state.
     * We need to start it again in order to reinitialise UniFi.
     */
    switch (status) {
        case unifi_Control_Error:
          unifi_trace(priv, UDBG1,
                      "unifi_sys_stopped_req: Restarting SME (ind:%d)\n",
                      status);

          /* On error, restart the SME */
          sme_mgt_wifi_on(priv);
          break;
        case unifi_Control_Exit:
          break;
        default:
          break;
    }

}


void unifi_mgt_set_value_cfm(void *drvpriv, unifi_Status status,
                             unifi_AppValueId ValueId)
{
    unifi_priv_t *priv = (unifi_priv_t*)drvpriv;

    if (priv == NULL) {
        unifi_error(NULL, "unifi_mgt_set_value_cfm: Invalid ospriv.\n");
        return;
    }

    sme_complete_request(priv, status);
}


void unifi_mgt_get_value_cfm(void *drvpriv, unifi_Status status,
                             const unifi_AppValue* appValue)
{
    unifi_priv_t *priv = (unifi_priv_t*)drvpriv;

    if (priv == NULL) {
        unifi_error(NULL, "unifi_mgt_get_value_cfm: Invalid ospriv.\n");
        return;
    }

    memcpy((unsigned char*)&priv->sme_reply.reply_app_value,
           (unsigned char*)appValue,
           sizeof(unifi_AppValue));

    sme_complete_request(priv, status);
}

void unifi_mgt_mib_set_cfm(void *drvpriv, unifi_Status status)
{
    unifi_priv_t *priv = (unifi_priv_t*)drvpriv;

    if (priv == NULL) {
        unifi_error(NULL, "unifi_mgt_mib_set_cfm: Invalid ospriv.\n");
        return;
    }

    sme_complete_request(priv, status);
}

void unifi_mgt_mib_get_cfm(void *drvpriv, unifi_Status status,
                           const unifi_DataBlock* mibAttributeValue)
{
    unifi_priv_t *priv = (unifi_priv_t*)drvpriv;

    if (priv == NULL) {
        unifi_error(NULL, "unifi_mgt_mib_get_cfm: Invalid ospriv.\n");
        return;
    }

    if (mibAttributeValue == NULL) {
        unifi_error(priv, "unifi_mgt_mib_get_cfm: Empty reply.\n");
        sme_complete_request(priv, status);
        return;
    }

    if ((priv->mib_cfm_buffer != NULL) &&
        (priv->mib_cfm_buffer_length >= mibAttributeValue->length)) {
        memcpy(priv->mib_cfm_buffer, mibAttributeValue->data, mibAttributeValue->length);
        priv->mib_cfm_buffer_length = mibAttributeValue->length;
    } else {
        unifi_error(priv,
                    "unifi_mgt_mib_get_cfm: No room to store MIB data (have=%d need=%d).\n",
                    priv->mib_cfm_buffer_length, mibAttributeValue->length);
    }

    sme_complete_request(priv, status);
}

void unifi_mgt_mib_get_next_cfm(void *drvpriv, unifi_Status status,
                                const unifi_DataBlock* mibAttribute)
{
    unifi_priv_t *priv = (unifi_priv_t*)drvpriv;

    if (priv == NULL) {
        unifi_error(NULL, "unifi_mgt_mib_get_next_cfm: Invalid ospriv.\n");
        return;
    }

    /* FIXME: Need to copy MIB data */

    sme_complete_request(priv, status);
}

void unifi_mgt_connection_quality_ind(void *drvpriv, const unifi_ConnectionStats* connectionStats)
{
    unifi_priv_t *priv = (unifi_priv_t*)drvpriv;
    int signal, noise, snr;

    if (priv == NULL) {
        unifi_error(NULL, "unifi_mgt_connection_quality_ind: Invalid ospriv.\n");
        return;
    }

    snr = connectionStats->unifiSNR;
    signal = connectionStats->unifiRSSI;
    noise = signal - snr;

    /* Clip range */
    signal = (signal < 63) ? signal : 63;
    signal = (signal > -192) ? signal : -192;

    noise = (noise < 63) ? noise : 63;
    noise = (noise > -192) ? noise : -192;

    /* Make u8 */
    signal = ( signal < 0 ) ? signal + 0x100 : signal;
    noise = ( noise < 0 ) ? noise + 0x100 : noise;

    priv->wext_wireless_stats.qual.level   = (u8)signal; /* -192 : 63 */
    priv->wext_wireless_stats.qual.noise   = (u8)noise;  /* -192 : 63 */
    priv->wext_wireless_stats.qual.qual    = snr;         /* 0 : 255 */
    priv->wext_wireless_stats.qual.updated = 0;
#if WIRELESS_EXT > 16
    priv->wext_wireless_stats.qual.updated |= IW_QUAL_LEVEL_UPDATED | IW_QUAL_NOISE_UPDATED |
    IW_QUAL_QUAL_UPDATED;
#if WIRELESS_EXT > 18
    priv->wext_wireless_stats.qual.updated |= IW_QUAL_DBM;
#endif
#endif

    priv->cached_tx_rate = connectionStats->unifiTxDataRate;
}

void unifi_mgt_packet_filter_set_cfm(void *drvpriv, unifi_Status status)
{
    unifi_priv_t *priv = (unifi_priv_t*)drvpriv;

    if (priv == NULL) {
        unifi_error(NULL, "unifi_mgt_packet_filter_set_cfm: Invalid ospriv.\n");
        return;
    }

    /* The packet filter set request does not block for a reply */
}

void unifi_mgt_tspec_cfm(void* drvpriv, unifi_Status status,
                         uint32 transactionId,
                         unifi_TspecResultCode tspecResultCode,
                         const unifi_DataBlock* tspec)
{
    unifi_priv_t *priv = (unifi_priv_t*)drvpriv;

    if (priv == NULL) {
        unifi_error(NULL, "unifi_mgt_tspec_cfm: Invalid ospriv.\n");
        return;
    }

    sme_complete_request(priv, status);
}

void unifi_mgt_tspec_ind(void* drvpriv, unifi_ListAction action, uint32 transactionId)
{
    unifi_priv_t *priv = (unifi_priv_t*)drvpriv;

    if (priv == NULL) {
        unifi_error(NULL, "unifi_mgt_tspec_ind: Invalid ospriv.\n");
        return;
    }


}


int sme_mgt_wifi_on(unifi_priv_t *priv)
{
/*    sme_reply_t *sme_reply_ptr;
    enum sme_request_status* request_status_ptr = NULL;
    int r;*/
    unifi_DataBlockList mib_data_list;

    if (priv->smepriv == NULL) {
        unifi_error(priv, "sme_mgt_wifi_on: invalid smepriv\n");
        return -EIO;
    }

#if 0
    r = sme_init_request(priv);
    if (r) {
        return -EIO;
    }
#endif

#ifdef CSR_SME_EMB
    /*
     * We need to read the MAC address, before we call unifi_mgt_wifi_on_req()
     */
    uf_request_mac_address_file(priv);
#endif

    if (priv->mib_data.length) {
        mib_data_list.numElements = 1;
        mib_data_list.datalist = &priv->mib_data;
    } else {
        mib_data_list.numElements = 0;
        mib_data_list.datalist = NULL;
    }
    /* Start the SME */
    unifi_mgt_wifi_on_req(priv->smepriv,
                          &priv->sta_mac_address,
                          &mib_data_list);
    return 0;

#if 0
    r = sme_wait_for_reply(priv, UNIFI_SME_MGT_LONG_TIMEOUT);
    if (r) {
        return r;
    }

    unifi_trace(priv, UDBG4,
                "sme_mgt_wifi_on: unifi_mgt_wifi_on_req <-- (%d)\n", r);
    return convert_sme_error(priv->sme_reply.reply_status);
#endif
} /* sme_mgt_wifi_on() */


int sme_mgt_wifi_off(unifi_priv_t *priv)
{
    int r;

    if (priv->smepriv == NULL) {
        unifi_error(priv, "sme_mgt_wifi_off: invalid smepriv\n");
        return -EIO;
    }

    r = sme_init_request(priv);
    if (r) {
        return -EIO;
    }

    /* Stop the SME */
    unifi_mgt_wifi_off_req(priv->smepriv);

    r = sme_wait_for_reply(priv, UNIFI_SME_MGT_LONG_TIMEOUT);
    if (r) {
        return r;
    }

    unifi_trace(priv, UDBG4,
                "sme_mgt_wifi_off: unifi_mgt_wifi_off_req <-- (%d)\n", r);
    return convert_sme_error(priv->sme_reply.reply_status);

} /* sme_mgt_wifi_off */


int sme_mgt_set_value_async(unifi_priv_t *priv, unifi_AppValue *app_value)
{
    int r;

    if (priv->smepriv == NULL) {
        unifi_error(priv, "sme_mgt_set_value_async: invalid smepriv\n");
        return -EIO;
    }

    r = sme_init_request(priv);
    if (r) {
        return -EIO;
    }

    unifi_mgt_set_value_req(priv->smepriv, app_value);

    r = sme_wait_for_reply(priv, UNIFI_SME_MGT_SHORT_TIMEOUT);
    if (r) {
        return r;
    }

    unifi_trace(priv, UDBG5,
                "sme_mgt_set_value_async: unifi_mgt_set_value_req <-- (%d)\n",
                r);
    return convert_sme_error(priv->sme_reply.reply_status);
} /* sme_mgt_set_value_async() */


int sme_mgt_get_value_async(unifi_priv_t *priv, unifi_AppValue *app_value)
{
    int r;

    if (priv->smepriv == NULL) {
        unifi_error(priv, "sme_app_get_value: invalid smepriv\n");
        return -EIO;
    }

    unifi_trace(priv, UDBG4, "sme_app_get_value: unifi_mgt_get_value_req -->\n");
    r = sme_init_request(priv);
    if (r) {
        return -EIO;
    }

    unifi_mgt_get_value_req(priv->smepriv, app_value->id);

    r = sme_wait_for_reply(priv, UNIFI_SME_MGT_SHORT_TIMEOUT);
    if (r) {
        return r;
    }

    /* store the reply */
    if (app_value != NULL) {
        memcpy((unsigned char*)app_value,
               (unsigned char*)&priv->sme_reply.reply_app_value,
               sizeof(unifi_AppValue));
    }

    unifi_trace(priv, UDBG4, "sme_app_get_value: unifi_mgt_set_value_req <-- (%d)\n", r);

    return convert_sme_error(priv->sme_reply.reply_status);
} /* sme_mgt_get_value_async() */


int sme_mgt_key(unifi_priv_t *priv, unifi_Key *sme_key,
                enum unifi_ListAction action)
{
    int r;

    if (priv->smepriv == NULL) {
        unifi_error(priv, "sme_mgt_key: invalid smepriv\n");
        return -EIO;
    }

    r = sme_init_request(priv);
    if (r) {
        return -EIO;
    }

    unifi_mgt_key_req(priv->smepriv, action, sme_key);

    r = sme_wait_for_reply(priv, UNIFI_SME_MGT_SHORT_TIMEOUT);
    if (r) {
        return r;
    }

    return convert_sme_error(priv->sme_reply.reply_status);
}


int sme_mgt_scan_full(unifi_priv_t *priv, unifi_SSID *specific_ssid)
{
    static const unifi_DataBlock empty_data_block = {0, NULL};
    int r;

    if (priv->smepriv == NULL) {
        unifi_error(priv, "sme_mgt_scan_full: invalid smepriv\n");
        return -EIO;
    }

    unifi_trace(priv, UDBG4, "sme_mgt_scan_full: -->\n");

    r = sme_init_request(priv);
    if (r) {
        return -EIO;
    }

    unifi_mgt_scan_full_req(priv->smepriv,
                            specific_ssid,
                            FALSE,
                            unifi_ScanAll,
                            &empty_data_block);
    r = sme_wait_for_reply(priv, UNIFI_SME_MGT_LONG_TIMEOUT);
    if (r) {
        return r;
    }

    unifi_trace(priv, UDBG4, "sme_mgt_scan_full: <-- (%d)\n", r);
    return convert_sme_error(priv->sme_reply.reply_status);
}


int sme_mgt_scan_results_get_async(unifi_priv_t *priv,
                                   struct iw_request_info *info,
                                   char *scan_results,
                                   long scan_results_len)
{
    unifi_ScanResultList scan_result_list;
    unifi_ScanResult *scan_result;
    int r;
    int i;
    char *current_ev = scan_results;

    if (priv->smepriv == NULL) {
        unifi_error(priv, "sme_mgt_scan_results_get_async: invalid smepriv\n");
        return -EIO;
    }

    r = sme_init_request(priv);
    if (r) {
        return -EIO;
    }

    unifi_mgt_scan_results_get_req(priv->smepriv);
    r = sme_wait_for_reply(priv, UNIFI_SME_MGT_LONG_TIMEOUT);
    if (r) {
        return r;
    }

    scan_result_list = priv->sme_reply.reply_scan_results;
    unifi_trace(priv, UDBG2,
                "scan_results: Scan returned %d, numElements=%d\n",
                r, scan_result_list.numElements);

    /* OK, now we have the scan results */
    for (i = 0; i < scan_result_list.numElements; ++i) {
        scan_result = &scan_result_list.results[i];

        unifi_trace(priv, UDBG2, "Scan Result: %.*s\n",
                    scan_result->ssid.length,
                    scan_result->ssid.ssid);

        r = unifi_translate_scan(priv->netdev, info,
                                 current_ev,
                                 scan_results + scan_results_len,
                                 scan_result, i+1);
        if (r < 0) {
            unifi_free(priv, scan_result_list.results);
            priv->sme_reply.reply_scan_results.numElements = 0;
            priv->sme_reply.reply_scan_results.results = NULL;
            return r;
        }

        current_ev += r;
    }

    /*
     * Free the scan results allocated in unifi_mgt_scan_results_get_cfm()
     * and invalidate the reply_scan_results to avoid re-using
     * the freed pointers.
     */
    unifi_free(priv, scan_result_list.results);
    priv->sme_reply.reply_scan_results.numElements = 0;
    priv->sme_reply.reply_scan_results.results = NULL;

    unifi_trace(priv, UDBG2,
                "scan_results: Scan translated to %d bytes\n",
                current_ev - scan_results);
    return (current_ev - scan_results);
}


int sme_mgt_connect(unifi_priv_t *priv)
{
    int r;

    if (priv->smepriv == NULL) {
        unifi_error(priv, "sme_mgt_connect: invalid smepriv\n");
        return -EIO;
    }

    unifi_trace(priv, UDBG2, "sme_mgt_connect: %.*s\n",
                priv->connection_config.ssid.length,
                priv->connection_config.ssid.ssid);

    r = sme_init_request(priv);
    if (r) {
        return -EIO;
    }

    unifi_mgt_connect_req(priv->smepriv, &priv->connection_config);
    r = sme_wait_for_reply(priv, UNIFI_SME_MGT_SHORT_TIMEOUT);
    if (r) {
        return r;
    }

    if (priv->sme_reply.reply_status) {
        unifi_trace(priv, UDBG1, "sme_mgt_connect: failed with SME status %d\n",
                    priv->sme_reply.reply_status);
    }   

    return convert_sme_error(priv->sme_reply.reply_status);
}


void unifi_mgt_media_status_ind(void *drvpriv,
                                unifi_MediaStatus mediaStatus,
                                const unifi_ConnectionInfo* connection_info)
{
    unifi_priv_t *priv = (unifi_priv_t*)drvpriv;

    if (priv->smepriv == NULL) {
        unifi_error(priv, "unifi_mgt_media_status_ind: invalid smepriv\n");
        return;
    }

    if (mediaStatus == unifi_MediaConnected) {
        /*
         * Send wireless-extension event up to userland to announce
         * connection.
         */
        wext_send_assoc_event(priv,
                              (unsigned char *)connection_info->assocReqApAddress.data,
                              (unsigned char *)connection_info->assocReqInfoElements.data,
                              connection_info->assocReqInfoElements.length,
                              (unsigned char *)connection_info->assocRspInfoElements.data,
                              connection_info->assocRspInfoElements.length,
                              (unsigned char *)connection_info->assocScanInfoElements.data,
                              connection_info->assocScanInfoElements.length);

        sme_mgt_packet_filter_set(priv);

    } else  {
        /*
         * Send wireless-extension event up to userland to announce
         * connection lost to a BSS.
         */
        wext_send_disassoc_event(priv);
    }
}


int sme_mgt_disconnect(unifi_priv_t *priv)
{
    int r;

    if (priv->smepriv == NULL) {
        unifi_error(priv, "sme_mgt_disconnect: invalid smepriv\n");
        return -EIO;
    }

    r = sme_init_request(priv);
    if (r) {
        return -EIO;
    }

    unifi_mgt_disconnect_req(priv->smepriv);
    r = sme_wait_for_reply(priv, UNIFI_SME_MGT_SHORT_TIMEOUT);
    if (r) {
        return r;
    }

    unifi_trace(priv, UDBG4, "sme_mgt_disconnect: <-- (%d)\n", r);
    return convert_sme_error(priv->sme_reply.reply_status);
}


int sme_mgt_pmkid(unifi_priv_t *priv,
                  unifi_ListAction action,
                  unifi_PmkidList *pmkid_list)
{
    int r;

    if (priv->smepriv == NULL) {
        unifi_error(priv, "sme_mgt_pmkid: invalid smepriv\n");
        return -EIO;
    }

    r = sme_init_request(priv);
    if (r) {
        return -EIO;
    }

    unifi_mgt_pmkid_req(priv->smepriv, action, pmkid_list);
    r = sme_wait_for_reply(priv, UNIFI_SME_MGT_SHORT_TIMEOUT);
    if (r) {
        return r;
    }

    unifi_trace(priv, UDBG4, "sme_mgt_pmkid: <-- (%d)\n", r);
    return convert_sme_error(priv->sme_reply.reply_status);
}


int sme_mgt_mib_get(unifi_priv_t *priv,
                    unsigned char *varbind, int *length)
{
    int r;
    unifi_DataBlock sme_data_block;

    if (priv->smepriv == NULL) {
        unifi_error(priv, "sme_mgt_mib_get: invalid smepriv\n");
        return -EIO;
    }

    r = sme_init_request(priv);
    if (r) {
        return -EIO;
    }

    sme_data_block.data = varbind;
    sme_data_block.length = *length;
    priv->mib_cfm_buffer = varbind;
    priv->mib_cfm_buffer_length = MAX_VARBIND_LENGTH;

    unifi_mgt_mib_get_req(priv->smepriv, &sme_data_block);
    r = sme_wait_for_reply(priv, UNIFI_SME_MGT_SHORT_TIMEOUT);
    if (r) {
        priv->mib_cfm_buffer_length = 0;
        priv->mib_cfm_buffer = NULL;
        return r;
    }

    *length = priv->mib_cfm_buffer_length;

    priv->mib_cfm_buffer_length = 0;
    priv->mib_cfm_buffer = NULL;
    unifi_trace(priv, UDBG4, "sme_mgt_mib_get: <-- (%d)\n", r);
    return convert_sme_error(priv->sme_reply.reply_status);
}

int sme_mgt_mib_set(unifi_priv_t *priv,
                    unsigned char *varbind, int length)
{
    int r;
    unifi_DataBlock sme_data_block;

    if (priv->smepriv == NULL) {
        unifi_error(priv, "sme_mgt_mib_get: invalid smepriv\n");
        return -EIO;
    }

    r = sme_init_request(priv);
    if (r) {
        return -EIO;
    }

    sme_data_block.data = varbind;
    sme_data_block.length = length;

    unifi_mgt_mib_set_req(priv->smepriv, &sme_data_block);
    r = sme_wait_for_reply(priv, UNIFI_SME_MGT_SHORT_TIMEOUT);
    if (r) {
        return r;
    }

    unifi_trace(priv, UDBG4, "sme_mgt_mib_set: <-- (%d)\n", r);
    return convert_sme_error(priv->sme_reply.reply_status);
}



int sme_mgt_get_versions(unifi_priv_t *priv, unifi_Versions *versions)
{
    unifi_AppValue sme_app_value;
    int r;

    sme_app_value.id = unifi_VersionsValue;
    r = sme_mgt_get_value(priv, &sme_app_value);
    if (r) {
        unifi_error(priv,
                    "sme_mgt_get_versions: Failed to get unifi_VersionsValue.\n");
        return r;
    }

    memcpy(versions, &sme_app_value.unifi_Value_union.versions, sizeof(unifi_Versions));

    return 0;
}


int sme_mgt_get_value(unifi_priv_t *priv, unifi_AppValue *app_value)
{
    unifi_Status status;
    if (priv->smepriv == NULL) {
        unifi_error(priv, "sme_mgt_get_value: invalid smepriv\n");
        return -EIO;
    }

#ifdef CSR_SME_USERSPACE
    status = sme_mgt_get_value_async(priv->smepriv, app_value);
#else
    /* FIXME : This needs some rework as the
     *         buffers in app_value need to be copied */
    unifi_mgt_claim_sync_access(priv->smepriv);
    status = unifi_mgt_get_value(priv->smepriv, app_value);
    unifi_mgt_release_sync_access(priv->smepriv);
#endif
    return status;
} /* sme_mgt_get_value() */


int sme_mgt_set_value(unifi_priv_t *priv, unifi_AppValue *app_value)
{
    unifi_Status status;
    if (priv->smepriv == NULL) {
        unifi_error(priv, "sme_mgt_set_value: invalid smepriv\n");
        return -EIO;
    }
#ifdef CSR_SME_USERSPACE
    status = sme_mgt_set_value_async(priv->smepriv, app_value);
#else
    unifi_mgt_claim_sync_access(priv->smepriv);
    status = unifi_mgt_set_value(priv->smepriv, app_value);
    unifi_mgt_release_sync_access(priv->smepriv);
#endif
    return status;
} /* sme_mgt_set_value() */


int sme_mgt_packet_filter_set(unifi_priv_t *priv)
{
    unifi_DataBlock filter;

    if (priv->smepriv == NULL) {
        unifi_error(priv, "sme_mgt_packet_filter_set: invalid smepriv\n");
        return -EIO;
    }

    filter.data = priv->filter_tclas_ies;
    filter.length = priv->packet_filters.tclas_ies_length;
    unifi_mgt_packet_filter_set_req(priv->smepriv, &filter,
                                    priv->packet_filters.filter_mode,
                                    (priv->packet_filters.arp_filter ? priv->sta_ip_address : 0xFFFFFFFF));
    return 0;
}


int sme_mgt_tspec(unifi_priv_t *priv, unifi_ListAction action,
                  uint32 tid, unifi_DataBlock *tspec, unifi_DataBlock *tclas)
{
    int r;

    if (priv->smepriv == NULL) {
        unifi_error(priv, "sme_mgt_tspec: invalid smepriv\n");
        return -EIO;
    }

    r = sme_init_request(priv);
    if (r) {
        return -EIO;
    }

    unifi_mgt_tspec_req(priv->smepriv, action, tid, TRUE,
                        tspec, tclas);
    r = sme_wait_for_reply(priv, UNIFI_SME_MGT_SHORT_TIMEOUT);
    if (r) {
        return r;
    }

    unifi_trace(priv, UDBG4, "sme_mgt_tspec: <-- (%d)\n", r);
    return convert_sme_error(priv->sme_reply.reply_status);
}


