/*
 * ---------------------------------------------------------------------------
 * FILE: xbv.h
 *
 * PURPOSE:
 *      Definitions and declarations for code to read XBV files - the UniFi
 *      firmware download file format.
 *
 * Copyright (C) 2005-2008 by Cambridge Silicon Radio Ltd.
 *
 * Refer to LICENSE.txt included with this source code for details on
 * the license terms.
 *
 * ---------------------------------------------------------------------------
 */
#ifndef __XBV_H__
#define __XBV_H__

#ifndef TEST
#include "driver/unifi.h"
#endif


struct VMEQ {
    unsigned long addr;
    uint16 mask;
    uint16 value;
};

struct VAND {
    unsigned int first;
    unsigned int count;
};

struct VERS {
    unsigned int num_vand;
};

struct FWDL {
    unsigned long dl_addr;
    unsigned int dl_size;
    unsigned int dl_offset;
};

struct FWOV {
    unsigned int dl_size;
    unsigned int dl_offset;
};

struct PTDL {
    unsigned int dl_size;
    unsigned int dl_offset;
};

#define MAX_VMEQ 64
#define MAX_VAND 64
#define MAX_FWDL 256
#define MAX_PTDL 256

/* An XBV1 file can either contain firmware or patches (at the
 * moment).  The 'mode' member of the xbv1_t structure tells us which
 * one is the case. */
typedef enum
{
    xbv_unknown,
    xbv_firmware,
    xbv_patch
} xbv_mode;

typedef struct {

    xbv_mode mode;

    /* Parts of a Firmware XBV1 */

    struct VMEQ vmeq[MAX_VMEQ];
    unsigned int num_vmeq;
    struct VAND vand[MAX_VAND];
    struct VERS vers;

    unsigned long slut_addr;

    /* F/W download image, possibly more than one part */
    struct FWDL fwdl[MAX_FWDL];
    int num_fwdl;

    /* F/W overlay image, add r not used */
    struct FWOV fwov;

    /* Parts of a Patch XBV1 */

    unsigned long build_id;

    struct PTDL ptdl[MAX_PTDL];
    int num_ptdl;

}  xbv1_t;


typedef int (*fwreadfn_t)(void *ospriv, void *dlpriv, int offset, void *buf, int len);

int xbv1_parse(card_t *card, fwreadfn_t readfn, void *dlpriv, xbv1_t *fwinfo);
int xbv1_read_slut(card_t *card, fwreadfn_t readfn, void *dlpriv, xbv1_t *fwinfo,
                   symbol_t *slut, int slut_len);



#endif /* __XBV_H__ */
