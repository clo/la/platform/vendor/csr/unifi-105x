/*
 * ---------------------------------------------------------------------------
 *  FILE:     card_udi.c
 * 
 *  PURPOSE:
 *      Maintain a list of callbacks to log UniFi exchanges to one or more
 *      debug/monitoring client applications.
 *
 * NOTES:
 *      Just call the UDI driver log fn directly for now.
 *      When done properly, each open() on the UDI device will install
 *      a log function. We will call all log fns whenever a signal is written
 *      to or read form the UniFi.
 * 
 * Copyright (C) 2005-2008 by Cambridge Silicon Radio Ltd.
 *
 * Refer to LICENSE.txt included with this source code for details on
 * the license terms.
 *
 * ---------------------------------------------------------------------------
 */
#include "driver/unifi.h"
#include "card.h"


/*
 * ---------------------------------------------------------------------------
 *  unifi_print_status
 *
 *      Print status info to given character buffer.
 * 
 *  Arguments:
 *      None.
 *
 *  Returns:
 *      None.
 * ---------------------------------------------------------------------------
 */
int
unifi_print_status(card_t *card, char *str)
{
    char *p = str;
    sdio_config_data_t *cfg;
    unsigned int i, n;

    p += unifi_sprintf(p, "Chip ID %u\n", card->chip_id);
    p += unifi_sprintf(p, "Chip Version %04X\n", card->chip_version);
    p += unifi_sprintf(p, "HIP v%u.%u\n",
                 (card->config_data.version >> 8) & 0xFF,
                 card->config_data.version & 0xFF);
    p += unifi_sprintf(p, "Build %lu: %s\n", card->build_id, card->build_id_string);


    cfg = &card->config_data;

    p += unifi_sprintf(p, "sdio ctrl offset          %u\n", cfg->sdio_ctrl_offset);
    p += unifi_sprintf(p, "fromhost sigbuf handle    %u\n", cfg->fromhost_sigbuf_handle);
    p += unifi_sprintf(p, "tohost_sigbuf_handle      %u\n", cfg->tohost_sigbuf_handle);
    p += unifi_sprintf(p, "num_fromhost_sig_frags    %u\n", cfg->num_fromhost_sig_frags);
    p += unifi_sprintf(p, "num_tohost_sig_frags      %u\n", cfg->num_tohost_sig_frags);
    p += unifi_sprintf(p, "num_fromhost_data_slots   %u\n", cfg->num_fromhost_data_slots);
    p += unifi_sprintf(p, "num_tohost_data_slots     %u\n", cfg->num_tohost_data_slots);
    p += unifi_sprintf(p, "data_slot_size            %u\n", cfg->data_slot_size);

    /* Added by protocol version 0x0001 */
    p += unifi_sprintf(p, "overlay_size              %u\n", (unsigned int)cfg->overlay_size);

    /* Added by protocol version 0x0300 */
    p += unifi_sprintf(p, "data_slot_round           %u\n", cfg->data_slot_round);
    p += unifi_sprintf(p, "sig_frag_size             %u\n", cfg->sig_frag_size);

    /* Added by protocol version 0x0300 */
    p += unifi_sprintf(p, "tohost_sig_pad            %u\n", cfg->tohost_signal_padding);

    p += unifi_sprintf(p, "\nInternal state:\n");

    p += unifi_sprintf(p, "fhsr: %u\n", card->from_host_signals_r);
    p += unifi_sprintf(p, "fhsw: %u\n", card->from_host_signals_w);
    p += unifi_sprintf(p, "thsr: %u\n", card->to_host_signals_r);
    p += unifi_sprintf(p, "thsw: %u\n", card->to_host_signals_w);
    p += unifi_sprintf(p, "fh buffer contains: %u signals, %u bytes\n",
                 card->fh_buffer.count,
                 card->fh_buffer.ptr - card->fh_buffer.buf);
    p += unifi_sprintf(p, "paused: %u\n", card->paused);

    p += unifi_sprintf(p, "fh command q: %u waiting, %u free of %u:\n",
                 q_slots_used(&card->fh_command_queue),
                 q_slots_free(&card->fh_command_queue),
                 UNIFI_SOFT_COMMAND_Q_LENGTH);
    for (i = 0; i < UNIFI_WME_NO_OF_QS; i++) {
        p += unifi_sprintf(p, "fh traffic q[%u]: %u waiting, %u free of %u:\n", i,
                     q_slots_used(&card->fh_traffic_queue[i]),
                     q_slots_free(&card->fh_traffic_queue[i]),
                     UNIFI_SOFT_TRAFFIC_Q_LENGTH);
    }

    p += unifi_sprintf(p, "fh data slots free: %u\n",
                 CardGetFreeFromHostDataSlots(card));
    p += unifi_sprintf(p, "From host data slots:");
    n = card->config_data.num_fromhost_data_slots;
    for (i = 0; i < n; i++) {
        p += unifi_sprintf(p, " %u", card->from_host_data[i].data_length);
    }
    p += unifi_sprintf(p, "\n");
    
    p += unifi_sprintf(p, "To host data slots:");
    n = card->config_data.num_tohost_data_slots;
    for (i = 0; i < n; i++) {
        p += unifi_sprintf(p, " %u", card->to_host_data[i].data_length);
    }
    p += unifi_sprintf(p, "\n");


    p += unifi_sprintf(p, "\nStats:\n");
    p += unifi_sprintf(p, "Total SDIO bytes: R=%llu W=%llu\n",
                 card->sdio_bytes_read,
                 card->sdio_bytes_written);

    p += unifi_sprintf(p, "Interrupts generated: %lu\n", card->unifi_interrupt_seq);

    return (p - str);
} /* unifi_print_status() */
